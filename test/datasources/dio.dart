
import 'package:dio/dio.dart';
import 'package:mockito/annotations.dart';

@GenerateNiceMocks([MockSpec<Dio>()])
import 'dio.mocks.dart';

MockDio mockDio = MockDio();