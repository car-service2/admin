import 'package:admin/data/datasources/remote/api/drivers_api_provider.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'dio.mocks.dart';

void main() {
  late DriverApiProvider driverApiProvider;
  late MockDio mockDio;

  setUp(() {
    mockDio = MockDio();
    driverApiProvider = DriverApiProvider(mockDio, 'http://test.com');
  });

  test('getDrivers method should return a list of DriverModel', () async {
    // Arrange: Set up the mock response for Dio
    final queryParams = {'status': 'active'};
    final driversJson = {
      'items': [
        {
          'id': 1,
          'name': 'John Doe',
          'login': 'john.doe',
          'phone': 123456789,
          'image': 'profile.png',
          'gender': 'male',
          'in_line': true,
          'percent': '75',
          'lat': '12.3456',
          'long': '78.91011',
          'car_type': {
            'id': 1,
            'name': 'Sedan',
            'seats': 4,
            'description': 'Comfortable and spacious'
          },
          'created_at': '2023-08-02T12:34:56Z',
        },
        {
          'id': 2,
          'name': 'Jane Smith',
          'login': 'jane.smith',
          'phone': 987654321,
          'image': 'avatar.png',
          'gender': 'female',
          'in_line': false,
          'percent': '50',
          'lat': '98.7654',
          'long': '12.3456',
          'car_type': {
            'id': 2,
            'name': 'SUV',
            'seats': 7,
            'description': 'Spacious and powerful'
          },
          'created_at': '2023-08-02T10:00:00Z',
        },
      ],
    };

    when(mockDio.get(
      'http://test.com/api/driver',
      queryParameters: {'status': 'active'},
    )).thenAnswer(
      (_) async => Response(
        data: driversJson,
        statusCode: 200,
        requestOptions: RequestOptions(),
      ),
    );

    // Act: Call the getDrivers method
    final drivers = await driverApiProvider.getDrivers(
      queryParams: queryParams,
    );

    // Assert: Check the results
    expect(drivers, isList);
    expect(drivers.length, 2);

    // Assertions for the first driver (John Doe)
    expect(drivers[0].id, 1);
    expect(drivers[0].name, 'John Doe');
    expect(drivers[0].login, 'john.doe');
    expect(drivers[0].phone, 123456789);
    expect(drivers[0].image, 'profile.png');
    expect(drivers[0].gender, 'male');
    expect(drivers[0].inLine, true);
    expect(drivers[0].percent, '75');
    expect(drivers[0].lat, '12.3456');
    expect(drivers[0].long, '78.91011');
    expect(drivers[0].createdAt, '2023-08-02T12:34:56Z');
    expect(drivers[0].carType?.id, 1);
    expect(drivers[0].carType?.name, 'Sedan');
    expect(drivers[0].carType?.description, 'Comfortable and spacious');

    // Assertions for the second driver (Jane Smith)
    expect(drivers[1].id, 2);
    expect(drivers[1].name, 'Jane Smith');
    expect(drivers[1].login, 'jane.smith');
    expect(drivers[1].phone, 987654321);
    expect(drivers[1].image, 'avatar.png');
    expect(drivers[1].gender, 'female');
    expect(drivers[1].inLine, false);
    expect(drivers[1].percent, '50');
    expect(drivers[1].lat, '98.7654');
    expect(drivers[1].long, '12.3456');
    expect(drivers[1].createdAt, '2023-08-02T10:00:00Z');
    expect(drivers[1].carType?.id, 2);
    expect(drivers[1].carType?.name, 'SUV');
    expect(drivers[1].carType?.description, 'Spacious and powerful');
  });

  test('getAll method should return a list of DriverModel', () async {
    // Arrange: Set up the mock response for Dio
    final driversJson = [
      {
        'id': 1,
        'name': 'John Doe',
        'login': 'john.doe',
        'phone': 123456789,
        'image': 'profile.png',
        'gender': 'male',
        'in_line': true,
        'percent': '75',
        'lat': '12.3456',
        'long': '78.91011',
        'car_type': {
          'id': 1,
          'name': 'Sedan',
          'seats': 4,
          'description': 'Comfortable and spacious'
        },
        'created_at': '2023-08-02T12:34:56Z',
      },
      {
        'id': 2,
        'name': 'Jane Smith',
        'login': 'jane.smith',
        'phone': 987654321,
        'image': 'avatar.png',
        'gender': 'female',
        'in_line': false,
        'percent': '50',
        'lat': '98.7654',
        'long': '12.3456',
        'car_type': {
          'id': 2,
          'name': 'SUV',
          'seats': 7,
          'description': 'Spacious and powerful'
        },
        'created_at': '2023-08-02T10:00:00Z',
      },
    ];

    when(mockDio.get('http://test.com/api/location/drivers')).thenAnswer(
      (_) async => Response(
        data: driversJson,
        statusCode: 200,
        requestOptions: RequestOptions(),
      ),
    );

    // Act: Call the getAll method
    final drivers = await driverApiProvider.getAll();

    // Assert: Check the results
    expect(drivers, isList);
    expect(drivers.length, 2);

    // Assertions for the first driver (John Doe)
    expect(drivers[0].id, 1);
    expect(drivers[0].name, 'John Doe');
    expect(drivers[0].login, 'john.doe');
    expect(drivers[0].phone, 123456789);
    expect(drivers[0].image, 'profile.png');
    expect(drivers[0].gender, 'male');
    expect(drivers[0].inLine, true);
    expect(drivers[0].percent, '75');
    expect(drivers[0].lat, '12.3456');
    expect(drivers[0].long, '78.91011');
    expect(drivers[0].createdAt, '2023-08-02T12:34:56Z');
    expect(drivers[0].carType?.id, 1);
    expect(drivers[0].carType?.name, 'Sedan');
    expect(drivers[0].carType?.description, 'Comfortable and spacious');

    // Assertions for the second driver (Jane Smith)
    expect(drivers[1].id, 2);
    expect(drivers[1].name, 'Jane Smith');
    expect(drivers[1].login, 'jane.smith');
    expect(drivers[1].phone, 987654321);
    expect(drivers[1].image, 'avatar.png');
    expect(drivers[1].gender, 'female');
    expect(drivers[1].inLine, false);
    expect(drivers[1].percent, '50');
    expect(drivers[1].lat, '98.7654');
    expect(drivers[1].long, '12.3456');
    expect(drivers[1].createdAt, '2023-08-02T10:00:00Z');
    expect(drivers[1].carType?.id, 2);
    expect(drivers[1].carType?.name, 'SUV');
    expect(drivers[1].carType?.description, 'Spacious and powerful');
  });
}
