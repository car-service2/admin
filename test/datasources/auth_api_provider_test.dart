import 'package:admin/data/datasources/remote/api/auth_api_provider.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'dio.mocks.dart';

// Mock the Dio class
void main() {
  late AuthApiProvider authApiProvider;
  late MockDio mockDio;

  setUp(() {
    mockDio = MockDio();
    authApiProvider = AuthApiProvider(mockDio, 'http://example.com');
  });

  test('login method should return a TokenModel', () async {
    final loginResponse = {
      'access_token': 'token123',
      'token_type': 'Bearer',
      'expires_in': 3600,
    };
    when(mockDio.post(any, data: {
      "email": "user@example.com",
      "password": "password",
    })).thenAnswer(
      (_) async => Response(
        data: loginResponse,
        statusCode: 401,
        requestOptions: RequestOptions(),
      ),
    );

    final tokenModel = await authApiProvider.login(
      login: 'user@example.com',
      password: 'password',
    );

    expect(tokenModel.accessToken, 'token123');
    expect(tokenModel.tokenType, 'Bearer');
    expect(tokenModel.expiresIn, 3600);
  });

  test('me method should return a UserModel', () async {
    final userResponse = {
      'id': 1,
      'name': 'John Doe',
      'email': 'john.doe@example.com',
    };
    when(mockDio.post(any)).thenAnswer(
      (_) async => Response(
        data: userResponse,
        statusCode: 200,
        requestOptions: RequestOptions(),
      ),
    );

    final userModel = await authApiProvider.me();

    expect(userModel.id, 1);
    expect(userModel.name, 'John Doe');
    expect(userModel.email, 'john.doe@example.com');
  });

  test('logout method should complete without errors', () async {
    when(mockDio.post(any)).thenAnswer(
      (_) async => Response(
        data: null,
        statusCode: 200,
        requestOptions: RequestOptions(),
      ),
    );

    await expectLater(authApiProvider.logout(), completion(equals(null)));
  });
}
