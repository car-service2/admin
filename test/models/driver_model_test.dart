import 'package:admin/data/models/driver.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('DriverModel should be created from JSON', () {
    final json = {
      'id': 1,
      'name': 'John Doe',
      'login': 'john.doe',
      'phone': 123456789,
      'image': 'profile.png',
      'gender': 'male',
      'in_line': true,
      'percent': '75',
      'lat': '12.3456',
      'long': '78.91011',
      'car_type': {
        'id': 1,
        'name': 'Sedan',
        'seats': 4,
        'description': 'Comfortable and spacious'
      },
      'created_at': '2023-08-02T12:34:56Z',
    };

    final driverModel = DriverModel.fromJson(json);

    expect(driverModel.id, 1);
    expect(driverModel.name, 'John Doe');
    expect(driverModel.login, 'john.doe');
    expect(driverModel.phone, 123456789);
    expect(driverModel.image, 'profile.png');
    expect(driverModel.gender, 'male');
    expect(driverModel.inLine, true);
    expect(driverModel.percent, '75');
    expect(driverModel.lat, '12.3456');
    expect(driverModel.long, '78.91011');
    expect(driverModel.createdAt, '2023-08-02T12:34:56Z');

    expect(driverModel.carType?.id, 1);
    expect(driverModel.carType?.name, 'Sedan');
    expect(driverModel.carType?.description, 'Comfortable and spacious');
  });

  test('CarTypeModel should be created from JSON', () {
    final json = {
      'id': 1,
      'name': 'Sedan',
      'seats': 4,
      'description': 'Comfortable and spacious',
    };

    final carTypeModel = CarTypeModel.fromJson(json);

    expect(carTypeModel.id, 1);
    expect(carTypeModel.name, 'Sedan');
    expect(carTypeModel.description, 'Comfortable and spacious');
  });
}
