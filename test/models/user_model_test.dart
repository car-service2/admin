import 'package:admin/data/models/user_model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('UserModel should be created from JSON', () {
    final json = {
      'id': 1,
      'name': 'John Doe',
      'email': 'john.doe@example.com',
    };

    final userModel = UserModel.fromJson(json);

    expect(userModel.id, 1);
    expect(userModel.name, 'John Doe');
    expect(userModel.email, 'john.doe@example.com');
  });
}
