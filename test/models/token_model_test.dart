import 'package:admin/data/models/token_model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('TokenModel should be created from JSON', () {
    final json = {
      'access_token': 'abc123',
      'token_type': 'Bearer',
      'expires_in': 3600,
    };

    final tokenModel = TokenModel.fromJson(json);

    expect(tokenModel.accessToken, 'abc123');
    expect(tokenModel.tokenType, 'Bearer');
    expect(tokenModel.expiresIn, 3600);
  });
}
