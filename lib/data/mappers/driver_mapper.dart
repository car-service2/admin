import '../../domain/entities/driver.dart';
import '../models/driver.dart';
import 'mapper.dart';

class DriverMapper implements Mapper<DriverEntity, DriverModel> {
  @override
  DriverEntity fromModel(DriverModel model) {
    return DriverEntity(
      id: model.id,
      blocked: model.blocked,
      name: model.name,
      login: model.login,
      phone: model.phone,
      image: model.image,
      gender: model.gender,
      inLine: model.inLine,
      percent: model.percent,
      stopTime: model.stopTime,
      lat: model.lat,
      long: model.long,
      carType: model.carType != null
          ? CarTypeEntity(
              id: model.carType!.id,
              name: model.carType!.name,
            )
          : null,
      createdAt: model.createdAt,
      seats: model.seats,
    );
  }

  @override
  DriverModel toModel(DriverEntity entity) {
    return DriverModel(
      id: entity.id,
      blocked: entity.blocked,
      name: entity.name,
      login: entity.login,
      phone: entity.phone,
      image: entity.image,
      gender: entity.gender,
      inLine: entity.inLine,
      percent: entity.percent,
      stopTime: entity.stopTime,
      lat: entity.lat,
      long: entity.long,
      carType: entity.carType != null
          ? CarTypeModel(
              id: entity.carType!.id,
              name: entity.carType!.name,
            )
          : null,
      createdAt: entity.createdAt,
    );
  }
}
