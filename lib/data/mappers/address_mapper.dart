import '../../domain/entities/address.dart';
import '../models/address_model.dart';
import 'mapper.dart';

class AddressMapper implements Mapper<AddressEntity, AddressModel> {
  @override
  AddressEntity fromModel(AddressModel model) {
    return AddressEntity(
      id: model.id,
      name: model.name,
      driversCount: model.driversCount,
    );
  }

  @override
  AddressModel toModel(AddressEntity entity) {
    return AddressModel(
      id: entity.id,
      name: entity.name,
      driversCount: entity.driversCount,
    );
  }
}
