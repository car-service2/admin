import '../../domain/entities/token.dart';
import '../models/token_model.dart';
import 'mapper.dart';

class TokenMapper implements Mapper<TokenEntity, TokenModel> {
  @override
  TokenEntity fromModel(TokenModel model) {
    return TokenEntity(
      accessToken: model.accessToken,
      tokenType: model.tokenType,
      expiresIn: model.expiresIn,
    );
  }

  @override
  TokenModel toModel(TokenEntity entity) => throw UnimplementedError();
}
