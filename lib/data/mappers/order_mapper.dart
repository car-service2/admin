import '../../domain/entities/driver.dart';
import '../../domain/entities/order.dart';
import '../../domain/entities/organization.dart';
import '../models/driver.dart';
import '../models/order.dart';
import '../models/organization.dart';
import 'mapper.dart';

class OrderMapper implements Mapper<OrderEntity, OrderModel> {
  final Mapper<OrganizationEntity, OrganizationModel> _organizationMapper;
  final Mapper<DriverEntity, DriverModel> _driverMapper;

  OrderMapper(this._organizationMapper, this._driverMapper);

  @override
  OrderEntity fromModel(OrderModel model) {
    var driverEntity =
        model.driver != null ? _driverMapper.fromModel(model.driver!) : null;
    var canceledDriverEntity = model.canceledDriver != null
        ? _driverMapper.fromModel(model.canceledDriver!)
        : null;

    var organizationEntity = model.organization != null
        ? _organizationMapper.fromModel(model.organization!)
        : null;

    return OrderEntity(
      id: model.id,
      waypoints: model.waypoints,
      date: model.date,
      driver: driverEntity,
      canceledDriver: canceledDriverEntity,
      status: model.status == null
          ? null
          : OrderStatusEntity.fromString(model.status!),
      customerName: model.customerName,
      customerPhone: model.customerPhone,
      orderValue: model.orderValue ?? 0,
      orderCreditValue: model.orderCreditValue ?? 0,
      orderTollValue: model.orderTollValue ?? 0,
      paymentType: model.paymentType,
      organization: organizationEntity,
      important: model.important,
      description: model.description,
      toll: model.toll,
      female: model.female,
    );
  }

  @override
  OrderModel toModel(OrderEntity entity) {
    var driverModel =
        entity.driver != null ? _driverMapper.toModel(entity.driver!) : null;
    var organizationModel = entity.organization != null
        ? _organizationMapper.toModel(entity.organization!)
        : null;

    return OrderModel(
      id: entity.id,
      waypoints: entity.waypoints,
      date: entity.date,
      driver: driverModel,
      status: entity.status?.toQueryParam(),
      customerName: entity.customerName,
      customerPhone: entity.customerPhone,
      orderValue: entity.orderValue,
      orderCreditValue: entity.orderCreditValue,
      orderTollValue: entity.orderTollValue,
      paymentType: entity.paymentType,
      organization: organizationModel,
      important: entity.important,
      toll: entity.toll,
      description: entity.description,
      female: entity.female,
    );
  }
}
