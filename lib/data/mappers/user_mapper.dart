import '../../domain/entities/user.dart';
import '../models/user_model.dart';
import 'mapper.dart';

class UserMapper implements Mapper<UserEntity, UserModel> {
  @override
  UserEntity fromModel(UserModel model) {
    return UserEntity(
      id: model.id,
      name: model.name,
      email: model.email,
    );
  }

  @override
  UserModel toModel(UserEntity entity) => throw UnimplementedError();
}
