import '../../domain/entities/organization.dart';
import '../models/organization.dart';
import 'mapper.dart';

class OrganizationMapper
    implements Mapper<OrganizationEntity, OrganizationModel> {
  @override
  OrganizationEntity fromModel(OrganizationModel model) {
    return OrganizationEntity(
      id: model.id,
      name: model.name,
    );
  }

  @override
  OrganizationModel toModel(OrganizationEntity entity) {
    return OrganizationModel(
      id: entity.id,
      name: entity.name,
    );
  }
}
