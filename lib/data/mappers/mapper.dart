export 'address_mapper.dart';
export 'driver_mapper.dart';
export 'order_mapper.dart';
export 'organization_mapper.dart';
export 'token_mapper.dart';
export 'user_mapper.dart';

abstract class Mapper<E, M> {
  E fromModel(M model);

  M toModel(E entity);
}
