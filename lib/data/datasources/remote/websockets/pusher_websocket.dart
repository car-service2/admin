import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:web_socket_channel/io.dart';

abstract class PusherWebSocket<T> {
  final String wsUrl;
  final String channel;
  final String event;

  late final IOWebSocketChannel _channel;

  Stream<T> get stream {
    return _channel.stream.where((data) {
      final json = jsonDecode(data);
      return json['channel'] == channel && json['event'] == event;
    }).map(mapStream);
  }

  PusherWebSocket({
    required this.channel,
    required this.event,
    required this.wsUrl,
  });

  void subscribe() {
    _channel = IOWebSocketChannel.connect(wsUrl);

    _channel.sink.add(jsonEncode({
      "event": "pusher:subscribe",
      "data": {"auth": "", "channel": channel}
    }));

    _channel.ready.then((value) => debugPrint("connected $this"));
  }

  Future<void> dispose() => _channel.sink.close();

  T mapStream(event);
}
