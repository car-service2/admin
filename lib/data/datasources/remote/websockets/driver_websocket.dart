import 'dart:convert';

import 'package:admin/data/datasources/remote/websockets/pusher_websocket.dart';
import 'package:admin/data/mappers/driver_mapper.dart';
import 'package:admin/data/models/driver.dart';
import 'package:admin/domain/entities/driver.dart';

class DriverWebSocket extends PusherWebSocket<DriverEntity> {
  final DriverMapper mapper;

  DriverWebSocket({
    required this.mapper,
    required super.channel,
    required super.event,
    required super.wsUrl,
  });

  @override
  DriverEntity mapStream(event) {
    var jsonData = jsonDecode(event);
    var json = jsonDecode(jsonData['data']);
    var driverModel = DriverModel.fromJson(json);
    return mapper.fromModel(driverModel);
  }
}
