import 'dart:convert';

import 'package:admin/data/datasources/remote/websockets/pusher_websocket.dart';
import 'package:admin/domain/entities/order.dart';

import '../../../mappers/order_mapper.dart';
import '../../../models/order.dart';

class OrderWebSocket extends PusherWebSocket<OrderEntity> {
  final OrderMapper mapper;

  OrderWebSocket({
    required this.mapper,
    required super.channel,
    required super.event,
    required super.wsUrl,
  });

  @override
  OrderEntity mapStream(event) {
    var jsonData = jsonDecode(event);
    var json = jsonDecode(jsonData['data']);
    var driverModel = OrderModel.fromJson(json);
    return mapper.fromModel(driverModel);
  }
}
