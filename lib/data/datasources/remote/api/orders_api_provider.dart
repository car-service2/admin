import 'package:admin/data/datasources/interfaces/orders_datasource.dart';
import 'package:admin/data/datasources/remote/api/api_provider.dart';
import 'package:admin/data/models/order.dart';

class OrdersApiProvider extends ApiProvider implements OrderDataSource {
  OrdersApiProvider(super.dio, super.baseUrl);

  @override
  Future<List<OrderModel>> getAll(Map<String, dynamic>? queryParams) async {
    final ordersResource = await get(
      "$baseUrl/api/order",
      queryParams: queryParams,
    );

    return List.from(ordersResource['items'])
        .map((e) => OrderModel.fromJson(e))
        .toList();
  }

  @override
  Future<OrderModel> create(OrderModel orderModel) async {
    var orderJson = await post(
      "$baseUrl/api/order",
      data: orderModel.toJson(),
    );

    return OrderModel.fromJson(orderJson);
  }

  @override
  Future<void> destroy(int id) async {
    await delete("$baseUrl/api/order/$id");
  }

  @override
  Future<OrderModel> update(OrderModel orderModel) async {
    var orderJson = await put(
      "$baseUrl/api/order/${orderModel.id}",
      data: orderModel.toJson(),
    );

    return OrderModel.fromJson(orderJson);
  }

  @override
  Future<List<OrderModel>> getDriverCurrentOrders(int id) async {
    final ordersResource = await get(
      "$baseUrl/api/driver/$id/current-orders",
    );

    return List.from(ordersResource)
        .map((e) => OrderModel.fromJson(e))
        .toList();
  }
}
