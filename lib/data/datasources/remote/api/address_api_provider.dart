import 'package:admin/data/datasources/interfaces/address_datasource.dart';
import 'package:admin/data/datasources/remote/api/api_provider.dart';
import 'package:admin/data/models/address_model.dart';

class AddressApiProvider extends ApiProvider implements AddressDataSource {
  AddressApiProvider(super.dio, super.baseUrl);

  @override
  Future<List<AddressModel>> getAddresses() async {
    final addressesJson = await get("$baseUrl/api/stop/address/drivers/count");

    return (addressesJson as List)
        .map((e) => AddressModel.fromJson(e))
        .toList();
  }
}
