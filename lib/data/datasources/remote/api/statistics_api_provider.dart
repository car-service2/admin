import 'package:admin/data/datasources/interfaces/statistics_datasource.dart';
import 'package:admin/data/datasources/remote/api/api_provider.dart';
import 'package:admin/data/models/statistic_model.dart';

class StatisticsApiProvider extends ApiProvider
    implements StatisticsDataSource {
  StatisticsApiProvider(super.dio, super.baseUrl);

  @override
  Future<StatisticModel> driver({
    required int driverId,
    required String startDate,
    required String endDate,
  }) async {
    final json = await get(
      "$baseUrl/api/statistics/driver",
      queryParams: {
        "driver_id": driverId,
        "start_date": startDate,
        "end_date": endDate,
      },
    );

    return StatisticModel(
      count: json['count'] ?? 0,
      credit: json['credit'] ?? 0,
      toll: json['toll'] ?? 0,
      cash: json['cash'] ?? 0,
      total: json['total'] ?? 0,
      percentage: json['percentage'] ?? 0,
    );
  }

  @override
  Future<StatisticModel> organization({
    required int organizationId,
    required String startDate,
    required String endDate,
  }) async {
    final json = await get(
      "$baseUrl/api/statistics/organization",
      queryParams: {
        "organization_id": organizationId,
        "start_date": startDate,
        "end_date": endDate,
      },
    );

    return StatisticModel(
      count: json['count'] ?? 0,
      credit: json['credit'] ?? 0,
      toll: json['toll'] ?? 0,
      cash: json['cash'] ?? 0,
      total: json['total'] ?? 0,
      percentage: json['percentage'] ?? 0,
    );
  }
}
