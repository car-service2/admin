import 'package:dio/dio.dart';

import '../../../models/token_model.dart';
import '../../../models/user_model.dart';
import '../../interfaces/auth_datasource.dart';
import 'api_provider.dart';

class AuthApiProvider extends ApiProvider implements AuthDataSource {
  AuthApiProvider(Dio dio, String baseUrl) : super(dio, baseUrl);

  @override
  Future<TokenModel> login({
    required String login,
    required String password,
  }) async {
    final json = await post<Map<String, dynamic>>(
      "$baseUrl/api/auth/login",
      data: {
        "email": login,
        "password": password,
      },
    );
    return TokenModel.fromJson(json);
  }

  @override
  Future<UserModel> me() async {
    final json = await post<Map<String, dynamic>>(
      "$baseUrl/api/auth/me",
    );
    UserModel user = UserModel.fromJson(json);
    return user;
  }

  @override
  Future<void> logout() async {
    await post("$baseUrl/api/auth/logout");
  }
}
