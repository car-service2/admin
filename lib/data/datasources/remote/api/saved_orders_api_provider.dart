import 'package:admin/data/datasources/interfaces/orders_datasource.dart';
import 'package:admin/data/datasources/remote/api/api_provider.dart';
import 'package:admin/data/models/order.dart';

class SavedOrdersApiProvider extends ApiProvider implements OrderDataSource {
  SavedOrdersApiProvider(super.dio, super.baseUrl);

  @override
  Future<List<OrderModel>> getAll(Map<String, dynamic>? queryParams) async {
    final ordersResource = await get(
      "$baseUrl/api/saved-order",
      queryParams: queryParams,
    );

    return List.from(ordersResource['items'])
        .map((e) => OrderModel.fromJson(e))
        .toList();
  }

  @override
  Future<OrderModel> create(OrderModel orderModel) async {
    var orderJson = await post(
      "$baseUrl/api/saved-order",
      data: {
        "order_id": orderModel.id,
      },
    );

    return OrderModel.fromJson(orderJson);
  }

  @override
  Future<void> destroy(int id) async {
    await delete("$baseUrl/api/saved-order/$id");
  }

  @override
  Future<OrderModel> update(OrderModel orderModel) async {
    var orderJson = await put(
      "$baseUrl/api/saved-order/${orderModel.id}",
      data: orderModel.toJson(),
    );

    return OrderModel.fromJson(orderJson);
  }

  @override
  Future<List<OrderModel>> getDriverCurrentOrders(int id) {
    // TODO: implement getDriverOrders
    throw UnimplementedError();
  }
}
