import 'package:admin/data/datasources/interfaces/organizations_datasource.dart';
import 'package:admin/data/datasources/remote/api/api_provider.dart';
import 'package:admin/data/models/organization.dart';

class OrganizationsApiProvider extends ApiProvider
    implements OrganizationsDataSource {
  OrganizationsApiProvider(
    super.dio,
    super.baseUrl,
  );

  @override
  Future<List<OrganizationModel>> getAll() async {
    final organizationsJson = await get("$baseUrl/api/organization");

    return List.of(organizationsJson)
        .map((e) => OrganizationModel.fromJson(e))
        .toList();
  }
}
