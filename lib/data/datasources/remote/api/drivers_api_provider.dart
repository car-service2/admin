import 'package:admin/data/datasources/remote/api/api_provider.dart';
import 'package:admin/data/models/driver.dart';

import '../../interfaces/drivers_datasource.dart';

class DriverApiProvider extends ApiProvider implements DriverDataSource {
  DriverApiProvider(super.dio, super.baseUrl);

  @override
  Future<List<DriverModel>> getDrivers({
    required Map<String, dynamic> queryParams,
  }) async {
    final driversJson = await get(
      "$baseUrl/api/driver",
      queryParams: queryParams,
    );

    return (driversJson['items'] as List)
        .map((e) => DriverModel.fromJson(e))
        .toList();
  }

  @override
  Future<List<DriverModel>> getAll() async {
    final driversJson = await get("$baseUrl/api/location/drivers");

    return (driversJson as List).map((e) => DriverModel.fromJson(e)).toList();
  }

  @override
  Future<bool> block(id) async {
    final response = await put("$baseUrl/api/driver/$id/block");
    return response['blocked'] as bool;
  }
}
