import '../../models/statistic_model.dart';

abstract class StatisticsDataSource {
  Future<StatisticModel> driver({
    required int driverId,
    required String startDate,
    required String endDate,
  });

  Future<StatisticModel> organization({
    required int organizationId,
    required String startDate,
    required String endDate,
  });
}
