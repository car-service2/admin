import 'package:admin/data/models/driver.dart';

abstract class DriverDataSource {
  Future<List<DriverModel>> getDrivers({
    required Map<String, dynamic> queryParams,
  });

  Future<List<DriverModel>> getAll();

  Future<bool> block(id);
}
