import '../../models/token_model.dart';
import '../../models/user_model.dart';

abstract class AuthDataSource {
  Future<TokenModel> login({
    required String login,
    required String password,
  });

  Future<UserModel> me();

  Future<void> logout();
}
