import '../../models/order.dart';

abstract class OrderDataSource {
  Future<List<OrderModel>> getAll(Map<String, dynamic>? queryParams);

  Future<List<OrderModel>> getDriverCurrentOrders(int id);

  Future<OrderModel> create(OrderModel orderModel);

  Future<OrderModel> update(OrderModel orderModel);

  Future<void> destroy(int id);
}