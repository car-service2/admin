import 'package:admin/data/models/organization.dart';

abstract class OrganizationsDataSource {
  Future<List<OrganizationModel>> getAll();
}
