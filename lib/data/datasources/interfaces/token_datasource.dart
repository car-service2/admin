abstract class TokenDataSource {
  String? getToken();

  Future<bool> setToken(String token);

  Future<bool> deleteToken();
}
