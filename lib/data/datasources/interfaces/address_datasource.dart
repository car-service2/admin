import 'package:admin/data/models/address_model.dart';

abstract class AddressDataSource {
  Future<List<AddressModel>> getAddresses();
}
