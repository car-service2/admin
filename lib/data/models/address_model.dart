class AddressModel {
  final int id;
  final String name;
  final String? driversCount;

  AddressModel({
    required this.id,
    required this.name,
    required this.driversCount,
  });

  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return AddressModel(
      id: json['id'],
      name: json['name'],
      driversCount: json['drivers_count'].toString(),
    );
  }
}
