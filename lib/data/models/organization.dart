class OrganizationModel {
  int id;
  String name;

  OrganizationModel({
    required this.id,
    required this.name,
  });

  factory OrganizationModel.fromJson(e) {
    return OrganizationModel(
      id: e['id'],
      name: e['name'],
    );
  }
}
