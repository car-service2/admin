import 'package:admin/data/models/driver.dart';

import 'organization.dart';
import 'waypoint.dart';

class OrderModel {
  final int id;
  final DriverModel? driver;
  final DriverModel? canceledDriver;
  final List<Waypoint> waypoints;
  final int date;
  final String? status;
  final String? customerName;
  final String customerPhone;
  final num? orderValue;
  final num? orderCreditValue;
  final num? orderTollValue;
  final String paymentType;
  final OrganizationModel? organization;
  final bool important;
  final bool toll;
  final String? description;
  final bool female;

  OrderModel({
    required this.id,
    this.driver,
    this.canceledDriver,
    required this.waypoints,
    required this.date,
    required this.status,
    required this.customerName,
    required this.customerPhone,
    required this.orderValue,
    required this.paymentType,
    required this.organization,
    required this.important,
    required this.toll,
    this.description,
    required this.female,
    required this.orderCreditValue,
    required this.orderTollValue,
  });

  // Фабрический метод для создания объекта Order из Map (JSON)
  factory OrderModel.fromJson(Map<String, dynamic> json) {
    return OrderModel(
      id: json['id'],
      driver:
          json['driver'] != null ? DriverModel.fromJson(json['driver']) : null,
      canceledDriver:
      json['canceled_driver'] != null ? DriverModel.fromJson(json['canceled_driver']) : null,
      waypoints: List<Waypoint>.from(
        json['waypoints'].map(
          (wp) => Waypoint(
            rt: wp['rt'] ?? 0,
            passengers: wp['passengers'] ?? 0,
            address: wp['address'] ?? "",
            latitude: double.tryParse(wp['latitude'].toString()) ?? 0.0,
            longitude: double.tryParse(wp['longitude'].toString()) ?? 0.0,
          ),
        ),
      ),
      date: json['date'] as int,
      status: json['status'],
      customerName: json['customer_name'],
      customerPhone: json['customer_phone'].toString(),
      orderValue: num.tryParse(json['order_value'].toString()),
      orderCreditValue: num.tryParse(json['order_credit_value'].toString()),
      orderTollValue: num.tryParse(json['order_toll_value'].toString()),
      paymentType: json['payment_type'] ?? '',
      organization: json['organization'] != null
          ? OrganizationModel(
              id: json['organization']['id'],
              name: json['organization']['name'],
            )
          : null,
      important: json['important'],
      toll: json['toll'],
      description: json['description'],
      female: bool.tryParse(json['female'].toString()) ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "driver_id": driver?.id,
      "waypoints": waypoints.map((e) => e.toJson()).toList(),
      "date": date,
      "customer_name": customerName,
      "customer_phone": customerPhone,
      "order_value": orderValue,
      "order_credit_value": orderCreditValue,
      "order_toll_value": orderTollValue,
      "payment_type": paymentType,
      "organization_id": organization?.id,
      "important": important,
      "toll": toll,
      "description": description,
      "female": female,
    };
  }
}
