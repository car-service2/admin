class DriverModel {
  final int id;
  final bool blocked;
  final String name;
  final String login;
  final int phone;
  final int? seats;
  final String image;
  final String gender;
  final bool inLine;
  final String percent;
  final String? lat;
  final String? long;
  final CarTypeModel? carType;
  final String? createdAt;
  final String? stopTime;

  DriverModel({
    required this.id,
    required this.blocked,
    required this.name,
    required this.login,
    required this.phone,
    required this.image,
    required this.gender,
    required this.inLine,
    required this.stopTime,
    required this.percent,
    required this.lat,
    required this.long,
    required this.carType,
    required this.createdAt,
    this.seats,
  });

  factory DriverModel.fromJson(Map<String, dynamic> json) {
    return DriverModel(
      id: json['id'] ?? 0,
      blocked: json['blocked'] as bool,
      name: json['name'] ?? "",
      login: json['login'] ?? "",
      phone: json['phone'] ?? 0,
      image: json['image'] ?? "",
      gender: json['gender'] ?? "",
      stopTime: json['stop_time']?.toString(),
      inLine: json['in_line'] ?? false,
      percent: json['percent'].toString(),
      lat: json['lat'].toString(),
      long: json['long'].toString(),
      seats: int.tryParse(json['seats'].toString()),
      carType: json['car_type'] != null
          ? CarTypeModel.fromJson(json['car_type'])
          : null,
      createdAt: json['created_at'],
    );
  }
}

class CarTypeModel {
  int id;
  String name;
  String? description;

  CarTypeModel({
    required this.id,
    required this.name,
    this.description,
  });

  factory CarTypeModel.fromJson(Map<String, dynamic> json) {
    return CarTypeModel(
      id: json['id'],
      name: json['name'],
      description: json['description'],
    );
  }
}
