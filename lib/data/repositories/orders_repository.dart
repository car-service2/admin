import 'package:admin/data/datasources/interfaces/orders_datasource.dart';
import 'package:admin/domain/entities/order.dart';
import 'package:admin/domain/repositories/orders_repository.dart';

import '../mappers/mapper.dart';
import '../models/order.dart';

class OrdersRepositoryImpl implements OrdersRepository {
  final OrderDataSource _orderDataSource;
  final Mapper<OrderEntity, OrderModel> _orderMapper;

  OrdersRepositoryImpl(this._orderDataSource, this._orderMapper);

  @override
  Future<List<OrderEntity>> getOrders({
    required Map<String, dynamic> queryParams,
  }) async {
    final orderModelList = await _orderDataSource.getAll(queryParams);

    return orderModelList.map((e) => _orderMapper.fromModel(e)).toList();
  }

  @override
  Future<void> delete(int id) async {
    await _orderDataSource.destroy(id);
  }

  @override
  Future<OrderEntity> create(OrderEntity order) async {
    final orderModel = _orderMapper.toModel(order);
    final newOrderModel = await _orderDataSource.create(orderModel);

    return _orderMapper.fromModel(newOrderModel);
  }

  @override
  Future<OrderEntity> update(OrderEntity order) async {
    var orderModel = await _orderDataSource.update(_orderMapper.toModel(order));
    return _orderMapper.fromModel(orderModel);
  }

  @override
  Future<List<OrderEntity>> getDriverCurrentOrders(int id) async {
    final orderModelList = await _orderDataSource.getDriverCurrentOrders(id);

    return orderModelList.map((e) => _orderMapper.fromModel(e)).toList();
  }
}
