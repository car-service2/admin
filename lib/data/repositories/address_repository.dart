import 'package:admin/data/datasources/interfaces/address_datasource.dart';
import 'package:admin/data/mappers/mapper.dart';
import 'package:admin/domain/entities/address.dart';

import '../../domain/repositories/address_repository.dart';
import '../models/address_model.dart';

class AddressRepositoryImpl implements AddressRepository {
  final AddressDataSource addressDataSource;
  final Mapper<AddressEntity, AddressModel> addressMapper;

  AddressRepositoryImpl(this.addressDataSource, this.addressMapper);

  @override
  Future<List<AddressEntity>> getAddresses() async {
    final addressModels = await addressDataSource.getAddresses();

    return addressModels
        .map((model) => addressMapper.fromModel(model))
        .toList();
  }
}
