import 'package:admin/data/datasources/interfaces/drivers_datasource.dart';
import 'package:admin/domain/entities/driver.dart';

import '../../domain/repositories/drivers_repository.dart';
import '../mappers/mapper.dart';
import '../models/driver.dart';

class DriversRepositoryImpl implements DriversRepository {
  final DriverDataSource _driverDataSource;
  final Mapper<DriverEntity, DriverModel> _driverMapper;

  DriversRepositoryImpl(
    this._driverDataSource,
    this._driverMapper,
  );

  @override
  Future<List<DriverEntity>> getPaginated({
    required Map<String, dynamic> queryParams,
  }) async {
    final results = await _driverDataSource.getDrivers(
      queryParams: queryParams,
    );

    var drivers = results
        .map((driverModel) => _driverMapper.fromModel(driverModel))
        .toList();

    return drivers;
  }

  @override
  Future<List<DriverEntity>> getAll() async {
    final results = await _driverDataSource.getAll();

    var drivers = results
        .map((driverModel) => _driverMapper.fromModel(driverModel))
        .toList();

    return drivers;
  }

  @override
  Future<bool> block(id) async {
    return await _driverDataSource.block(id);
  }
}
