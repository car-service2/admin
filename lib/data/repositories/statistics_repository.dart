import 'package:admin/domain/entities/statistic.dart';
import 'package:admin/domain/repositories/statistics_repository.dart';

import '../datasources/interfaces/statistics_datasource.dart';

class StatisticsRepositoryImpl implements StatisticsRepository {
  final StatisticsDataSource _statisticsDataSource;

  StatisticsRepositoryImpl(this._statisticsDataSource);

  @override
  Future<StatisticEntity> driver({
    required int driverId,
    required String startDate,
    required String endDate,
  }) async {
    final model = await _statisticsDataSource.driver(
      driverId: driverId,
      startDate: startDate,
      endDate: endDate,
    );

    return StatisticEntity(
      count: model.count,
      credit: model.credit,
      cash: model.cash,
      total: model.total,
      percentage: model.percentage,
      toll: model.toll,
    );
  }

  @override
  Future<StatisticEntity> organization({
    required int organizationId,
    required String startDate,
    required String endDate,
  }) async {
    final model = await _statisticsDataSource.organization(
      organizationId: organizationId,
      startDate: startDate,
      endDate: endDate,
    );

    return StatisticEntity(
      count: model.count,
      credit: model.credit,
      cash: model.cash,
      total: model.total,
      percentage: model.percentage,
      toll: model.toll,
    );
  }
}
