import 'package:admin/data/datasources/interfaces/organizations_datasource.dart';
import 'package:admin/data/mappers/mapper.dart';
import 'package:admin/domain/repositories/organization_repository.dart';

import '../../domain/entities/organization.dart';
import '../models/organization.dart';

class OrganizationRepositoryImpl implements OrganizationRepository {
  final OrganizationsDataSource _organizationsDataSource;
  final Mapper<OrganizationEntity, OrganizationModel> _mapper;

  OrganizationRepositoryImpl(this._organizationsDataSource, this._mapper);

  @override
  Future<List<OrganizationEntity>> getAll() async {
    final organizationModels = await _organizationsDataSource.getAll();
    return organizationModels.map((e) => _mapper.fromModel(e)).toList();
  }
}
