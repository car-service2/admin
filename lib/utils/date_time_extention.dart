import 'package:intl/intl.dart';

extension Format on DateTime {
  String toMMMdy() {
    final DateFormat formatter = DateFormat("MMM d, y | ")..add_jm();
    return formatter.format(this);
  }

  String toYMD() {
    return DateFormat("y-mm-dd").format(this);
  }

  String format(pattern) {
    return DateFormat(pattern).format(this);
  }
}
