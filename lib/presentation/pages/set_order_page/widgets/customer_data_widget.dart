import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import 'call_log_bottom_sheet.dart';

class CustomerDataWidget extends StatefulWidget {
  final String? initialName;
  final String? initialPhone;
  final void Function(String value) onFullNameChanged;
  final void Function(String value) onPhoneChanged;

  const CustomerDataWidget({
    super.key,
    required this.onFullNameChanged,
    required this.onPhoneChanged,
    this.initialName,
    this.initialPhone,
  });

  @override
  State<CustomerDataWidget> createState() => _CustomerDataWidgetState();
}

class _CustomerDataWidgetState extends State<CustomerDataWidget> {
  final TextEditingController fullNameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();

  @override
  void initState() {
    fullNameController.text = widget.initialName ?? '';
    phoneController.text = widget.initialPhone ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(
        vertical: 14,
        horizontal: 15,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(18),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Customer data",
            style: GoogleFonts.montserrat(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(height: 20),
          IconTextField(
            icon: SvgPicture.asset(
              "assets/icons/user-line.svg",
              width: 20,
              height: 20,
            ),
            controller: fullNameController,
            hint: 'Full name',
            keyboardType: TextInputType.name,
            onChanged: widget.onFullNameChanged,
          ),
          const SizedBox(height: 10),
          IconTextField(
            icon: SvgPicture.asset(
              "assets/icons/phone-line.svg",
              width: 20,
              height: 20,
            ),
            controller: phoneController,
            hint: 'Phone number',
            keyboardType: TextInputType.phone,
            onChanged: widget.onPhoneChanged,
            suffixIcon: Platform.isAndroid
                ? GestureDetector(
                    onTap: () async {
                      final number =
                          await CallLogBottomSheet.show<String>(context);
                      if (number != null && number.isNotEmpty) {
                        phoneController.text = number;
                        widget.onPhoneChanged(phoneController.text);
                      }
                    },
                    child: const SizedBox.square(
                      dimension: 20,
                      child: Center(
                        child: Icon(
                          Icons.list,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  )
                : null,
          ),
        ],
      ),
    );
  }
}

class IconTextField extends StatefulWidget {
  final Widget icon;
  final TextEditingController controller;
  final String hint;
  final TextInputType keyboardType;
  final Function(String value) onChanged;
  final Widget? suffixIcon;

  const IconTextField({
    super.key,
    required this.icon,
    required this.controller,
    required this.hint,
    required this.keyboardType,
    required this.onChanged,
    this.suffixIcon,
  });

  @override
  State<IconTextField> createState() => _IconTextFieldState();
}

class _IconTextFieldState extends State<IconTextField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      keyboardType: widget.keyboardType,
      onChanged: widget.onChanged,
      decoration: InputDecoration(
        filled: true,
        fillColor: const Color(0xFFF6F6F6),
        prefixIcon: SizedBox.square(
          dimension: 20,
          child: Center(child: widget.icon),
        ),
        suffixIcon: widget.suffixIcon,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide.none,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(color: Colors.black),
        ),
        hintText: widget.hint,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 13,
          vertical: 16,
        ),
      ),
    );
  }
}
