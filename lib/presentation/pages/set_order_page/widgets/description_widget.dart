import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DescriptionWidget extends StatefulWidget {
  final String? initialDescription;
  final void Function(String value) onChanged;

  const DescriptionWidget({
    super.key,
    required this.onChanged,
    this.initialDescription,
  });

  @override
  State<DescriptionWidget> createState() => _DescriptionWidgetState();
}

class _DescriptionWidgetState extends State<DescriptionWidget> {
  final TextEditingController descriptionController = TextEditingController();

  @override
  void initState() {
    descriptionController.text = widget.initialDescription ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(
        vertical: 14,
        horizontal: 15,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(18),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Description",
            style: GoogleFonts.montserrat(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(height: 20),
          TextFormField(
            controller: descriptionController,
            keyboardType: TextInputType.text,
            maxLines: 50,
            minLines: 4,
            onChanged: widget.onChanged,
            decoration: InputDecoration(
              filled: true,
              fillColor: const Color(0xFFF6F6F6),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
                borderSide: const BorderSide(color: Colors.black),
              ),
              hintText: "Describe a trip",
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 13,
                vertical: 16,
              ),
            ),
          )
        ],
      ),
    );
  }
}
