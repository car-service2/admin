import 'package:call_log/call_log.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CallLogBottomSheet extends StatefulWidget {
  final ScrollController controller;

  static Future<String?> show<String>(BuildContext context) async {
    return await showModalBottomSheet(
      backgroundColor: Colors.white,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      showDragHandle: true,
      context: context,
      builder: (_) {
        return DraggableScrollableSheet(
          builder: (_, controller) {
            return CallLogBottomSheet(
              controller: controller,
            );
          },
          initialChildSize: 0.5,
          expand: false,
        );
      },
    );
  }

  const CallLogBottomSheet({
    super.key,
    required this.controller,
  });

  @override
  State<CallLogBottomSheet> createState() => _CallLogBottomSheetState();
}

class _CallLogBottomSheetState extends State<CallLogBottomSheet> {
  List<CallLogEntry> _callLogs = [];

  @override
  void initState() {
    super.initState();
    _loadCallLogs();
  }

  // Функция для получения журнала звонков
  void _loadCallLogs() async {
    try {
      Iterable<CallLogEntry> entries = await CallLog.get();
      setState(() {
        _callLogs = entries.toList();
      });
    } catch (e) {
      debugPrint('Error loading call logs: $e');
    }
  }

  // Функция для выбора звонка
  void _selectCall(CallLogEntry entry) {
    Navigator.pop(context, entry.number);
  }

  String _formatDateTime(int? timestamp) {
    if (timestamp == null) {
      return "";
    }

    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp);

    return '${dateTime.day}/${dateTime.month}/${dateTime.year} ${dateTime.hour}:${dateTime.minute.toString().padLeft(2, '0')}';
  }

  // Функция для получения иконки в зависимости от типа звонка
  Icon _getCallTypeIcon(CallType? callType) {
    switch (callType) {
      case CallType.incoming:
        return const Icon(Icons.call_received, color: Colors.green);
      case CallType.outgoing:
        return const Icon(Icons.call_made, color: Colors.blue);
      case CallType.missed:
        return const Icon(Icons.call_missed, color: Colors.red);
      case CallType.voiceMail:
        return const Icon(Icons.voicemail, color: Colors.purple);
      case CallType.rejected:
        return const Icon(Icons.call_end, color: Colors.red);
      case CallType.blocked:
        return const Icon(Icons.block, color: Colors.red);
      case CallType.answeredExternally:
        return const Icon(Icons.check, color: Colors.green);
      case CallType.unknown:
        return const Icon(Icons.help, color: Colors.grey);
      case CallType.wifiIncoming:
        return const Icon(Icons.wifi_calling, color: Colors.green);
      case CallType.wifiOutgoing:
        return const Icon(Icons.wifi_calling_3, color: Colors.blue);
      default:
        return const Icon(Icons.help, color: Colors.grey);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            "Select number",
            style: GoogleFonts.montserrat(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        const Divider(),
        Expanded(
          child: ListView.builder(
            controller: widget.controller,
            itemCount: _callLogs.length,
            itemBuilder: (context, index) {
              final entry = _callLogs[index];
              return ListTile(
                leading: _getCallTypeIcon(entry.callType),
                title: Text(entry.number ?? "-"),
                subtitle: Text(_formatDateTime(entry.timestamp)),
                onTap: () => _selectCall(entry),
              );
            },
          ),
        ),
      ],
    );
  }
}
