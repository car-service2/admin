import 'package:admin/domain/entities/organization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../organizations_page.dart';
import 'customer_data_widget.dart';

class PaymentTypeWidget extends StatefulWidget {
  final OrganizationEntity? initialOrganization;
  final String? initialCashValue;
  final String? initialCreditValue;
  final String? initialTollValue;
  final void Function(String value) onCashChanged;
  final void Function(String value) onCreditChanged;
  final void Function(String value) onTollChanged;
  final void Function(String value, {OrganizationEntity? organization})
      onChangedType;

  const PaymentTypeWidget({
    super.key,
    required this.onCashChanged,
    required this.onChangedType,
    this.initialOrganization,
    this.initialCashValue,
    required this.onCreditChanged,
    required this.onTollChanged,
    this.initialCreditValue,
    this.initialTollValue,
  });

  @override
  State<PaymentTypeWidget> createState() => _PaymentTypeWidgetState();
}

class _PaymentTypeWidgetState extends State<PaymentTypeWidget> {
  final TextEditingController cashController = TextEditingController();
  final TextEditingController creditController = TextEditingController();
  final TextEditingController tollController = TextEditingController();
  OrganizationEntity? organization;

  bool get isCashPaymentType => organization == null;

  @override
  void initState() {
    organization = widget.initialOrganization;
    cashController.text = widget.initialCashValue ?? '';
    creditController.text = widget.initialCreditValue ?? '';
    tollController.text = widget.initialTollValue ?? '';
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    creditController.dispose();
    cashController.dispose();
    tollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(
        vertical: 14,
        horizontal: 15,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(18),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Payment type",
            style: GoogleFonts.montserrat(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(height: 20),
          GestureDetector(
            onTap: setTypeCredit,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset("assets/icons/bank-card-line.svg"),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Text(
                      "Credit card",
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  organization != null
                      ? const Icon(
                          Icons.radio_button_checked,
                          color: Color(0xFF57BF9C),
                        )
                      : const Icon(
                          Icons.radio_button_off,
                          color: Colors.black,
                        ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: setTypeCash,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset("assets/icons/money-dollar-circle-line.svg"),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Text(
                      "Cash",
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  organization == null
                      ? const Icon(
                          Icons.radio_button_checked,
                          color: Color(0xFF57BF9C),
                        )
                      : const Icon(
                          Icons.radio_button_off,
                          color: Colors.black,
                        ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 20),
          IconTextField(
            icon: const Icon(Icons.money_outlined),
            controller: cashController,
            hint: 'Cash',
            keyboardType: TextInputType.number,
            onChanged: widget.onCashChanged,
          ),
          if (organization != null) ...[
            const SizedBox(height: 10),
            IconTextField(
              icon: const Icon(Icons.credit_card),
              controller: creditController,
              hint: 'Credit',
              keyboardType: TextInputType.number,
              onChanged: widget.onCreditChanged,
            ),
            const SizedBox(height: 10),
            IconTextField(
              icon: const Icon(Icons.toll_sharp),
              controller: tollController,
              hint: 'Toll',
              keyboardType: TextInputType.number,
              onChanged: widget.onTollChanged,
            ),
            const SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: const Color(0xFFF0FFFA),
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: const Color(0xFF57BF9C)),
              ),
              child: Row(
                children: [
                  const Icon(Icons.business, color: Color(0xFF57BF9C)),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Text(
                      "${organization?.name}",
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: const Color(0xFF57BF9C),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ]
        ],
      ),
    );
  }

  void setTypeCash() async {
    widget.onTollChanged("");
    widget.onCreditChanged("");

    creditController.clear();
    tollController.clear();

    organization = null;

    if (organization != null) {
      widget.onChangedType(
        "credit",
        organization: organization,
      );
    }

    setState(() => organization);
  }

  void setTypeCredit() async {
    widget.onTollChanged("");
    widget.onCreditChanged("");

    creditController.clear();
    tollController.clear();

    organization =
        await OrganizationsBottomSheet.show<OrganizationEntity>(context);

    if (organization != null) {
      widget.onChangedType(
        "credit",
        organization: organization,
      );
    }

    setState(() => organization);
  }
}
