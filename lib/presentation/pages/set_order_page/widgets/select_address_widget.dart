import 'package:admin/data/models/waypoint.dart';
import 'package:admin/presentation/pages/set_order_page/widgets/section.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../set_waypoint_page.dart';

class SelectWaypointsWidget extends StatefulWidget {
  final List<Waypoint> waypoints;
  final void Function(List<Waypoint> waypoints) onChanged;

  const SelectWaypointsWidget({
    super.key,
    required this.waypoints,
    required this.onChanged,
  });

  @override
  State<SelectWaypointsWidget> createState() => _SelectWaypointsWidgetState();
}

class _SelectWaypointsWidgetState extends State<SelectWaypointsWidget> {
  List<Waypoint> waypoints = [];

  @override
  void initState() {
    waypoints = widget.waypoints;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Section(
      title: 'Waypoints',
      children: [
        ...waypoints.map((wp) {
          final iconPath = wp == waypoints.first
              ? "assets/icons/focus-3-line.svg"
              : "assets/icons/map-pin-2-line.svg";
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              children: [
                SvgPicture.asset(
                  iconPath,
                  width: 18,
                  height: 18,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: const Color(0xFFF6F6F6),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("Address: ${wp.address}"),
                        Text("Round trip: ${wp.rt.toString()}"),
                        Text("Passengers: ${wp.passengers.toString()}"),
                      ],
                    ),
                  ),
                ),
                WaypointActionsPopup(
                  onDelete: () {
                    setState(() => waypoints.remove(wp));
                    widget.onChanged(waypoints);
                  },
                  onEdit: () async {
                    final Waypoint? editedWaypoint =
                        await Navigator.push<Waypoint>(
                      context,
                      MaterialPageRoute(
                        builder: (_) => SetWaypointPage(
                          waypoint: wp,
                        ),
                      ),
                    );

                    if (editedWaypoint != null) {
                      var index = waypoints.indexOf(wp);
                      waypoints[index] = editedWaypoint;
                      widget.onChanged(waypoints);
                      setState(() {});
                    }
                  },
                ),
              ],
            ),
          );
        }).toList(),
        ElevatedButton(
          style: TextButton.styleFrom(
            elevation: 0.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            minimumSize: const Size.fromHeight(40),
            maximumSize: const Size.fromHeight(40),
            backgroundColor: const Color(0xFF57BF9C),
          ),
          onPressed: () async {
            final wp = await Navigator.push<Waypoint>(
              context,
              MaterialPageRoute(
                builder: (_) => const SetWaypointPage(),
              ),
            );

            if (wp != null) {
              waypoints.add(wp);
              setState(() {});
              widget.onChanged(waypoints);
            }
          },
          child: const Text("Add waypoint"),
        )
      ],
    );
  }
}

class WaypointActionsPopup extends StatelessWidget {
  final VoidCallback onDelete;
  final VoidCallback onEdit;

  const WaypointActionsPopup({
    super.key,
    required this.onDelete,
    required this.onEdit,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 20,
      child: PopupMenuButton<String>(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        icon: const Icon(Icons.more_vert),
        itemBuilder: (_) {
          return [
            PopupMenuItem(
              onTap: () {
                WidgetsBinding.instance.addPostFrameCallback((_) => onEdit());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Edit",
                    style: GoogleFonts.montserrat(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: Colors.blue,
                    ),
                  ),
                  const Icon(Icons.edit, color: Colors.blue, size: 20),
                ],
              ),
            ),
            PopupMenuItem(
              onTap: () {
                WidgetsBinding.instance.addPostFrameCallback((_) => onDelete());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Delete",
                    style: GoogleFonts.montserrat(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: Colors.red,
                    ),
                  ),
                  const Spacer(),
                  const Icon(Icons.delete, color: Colors.red, size: 20),
                ],
              ),
            ),
          ];
        },
      ),
    );
  }
}
