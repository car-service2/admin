import 'package:admin/domain/entities/driver.dart';
import 'package:admin/presentation/pages/set_order_page/widgets/section.dart';
import 'package:admin/presentation/widgets/select_driver_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class ChooseDriverWidget extends StatefulWidget {
  final DriverEntity? driver;
  final void Function(DriverEntity? driver) onChange;

  const ChooseDriverWidget({
    super.key,
    required this.onChange,
    required this.driver,
  });

  @override
  State<ChooseDriverWidget> createState() => _ChooseDriverWidgetState();
}

class _ChooseDriverWidgetState extends State<ChooseDriverWidget> {
  DriverEntity? driver;

  @override
  void initState() {
    super.initState();
    driver = widget.driver;
  }

  @override
  Widget build(BuildContext context) {
    return Section(
      title: null,
      children: [
        InkWell(
          onTap: () async {
            driver = await SelectDriverBottomSheet.show<DriverEntity>(context);
            setState(() {});
            widget.onChange(driver);
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Choose a driver",
                style: GoogleFonts.montserrat(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SvgPicture.asset("assets/icons/arrow-right-s-line.svg"),
            ],
          ),
        ),
        if (driver != null) ...[
          const SizedBox(height: 20),
          Container(
            padding: const EdgeInsets.symmetric(
              vertical: 16,
              horizontal: 14,
            ),
            decoration: BoxDecoration(
              color: const Color(0xFFF6F6F6),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Row(
              children: [
                SvgPicture.asset(
                  "assets/icons/car-line.svg",
                  height: 20,
                  width: 20,
                ),
                const SizedBox(width: 14),
                Expanded(
                  child: Text(
                    driver!.name,
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    driver = null;
                    widget.onChange(driver);
                    setState(() {});
                  },
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ]
      ],
    );
  }
}
