import 'package:admin/domain/entities/driver.dart';
import 'package:admin/domain/entities/order.dart';
import 'package:admin/presentation/blocs/create_order/create_order_cubit.dart';
import 'package:admin/presentation/pages/set_order_page/widgets/section.dart';
import 'package:admin/presentation/theme/app_theme.dart';
import 'package:admin/presentation/widgets/button_widget.dart';
import 'package:admin/service_locator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

import '../../../data/models/waypoint.dart';
import '../../../domain/entities/organization.dart';
import '../../blocs/orders/orders_cubit.dart';
import 'widgets/choose_driver_widget.dart';
import 'widgets/customer_data_widget.dart';
import 'widgets/description_widget.dart';
import 'widgets/payment_type_widget.dart';
import 'widgets/select_address_widget.dart';
import 'widgets/set_date_widget.dart';

enum SetOrderPageType {
  createOrder,
  updateOrder,
  createOrderFromSaved,
  updateSavedOrder;

  @override
  String toString() {
    return switch (this) {
      SetOrderPageType.createOrder => 'Create New Order',
      SetOrderPageType.updateOrder => 'Update Order',
      SetOrderPageType.createOrderFromSaved => 'Create From Saved Order',
      SetOrderPageType.updateSavedOrder => 'Update Saved Order',
    };
  }
}

class SetOrderPage extends StatefulWidget {
  final OrderEntity? order;
  final SetOrderPageType type;

  const SetOrderPage._({
    this.order,
    required this.type,
  });

  static Future<T?> createOrder<T>(BuildContext context) async {
    return await Navigator.push<T>(
      context,
      MaterialPageRoute(
        builder: (_) => BlocProvider(
          create: (_) => locator.get<CreateOrderCubit>(),
          child: const SetOrderPage._(type: SetOrderPageType.createOrder),
        ),
      ),
    );
  }

  static Future<T?> updateOrder<T>(
    BuildContext context, {
    required OrderEntity order,
  }) async {
    return await Navigator.push<T>(
      context,
      MaterialPageRoute(
        builder: (_) => BlocProvider(
          create: (_) => locator.get<CreateOrderCubit>(),
          child: SetOrderPage._(
            type: SetOrderPageType.updateOrder,
            order: order,
          ),
        ),
      ),
    );
  }

  static Future<T?> createFromSavedOrder<T>(
    BuildContext context, {
    required OrderEntity savedOrder,
  }) async {
    return await Navigator.push<T>(
      context,
      MaterialPageRoute(
        builder: (_) => BlocProvider(
          create: (_) => locator.get<CreateOrderCubit>(),
          child: SetOrderPage._(
            type: SetOrderPageType.createOrderFromSaved,
            order: savedOrder,
          ),
        ),
      ),
    );
  }

  static Future<T?> updateSavedOrder<T>(
    BuildContext context, {
    required OrderEntity savedOrder,
  }) async {
    return await Navigator.push<T>(
      context,
      MaterialPageRoute(
        builder: (_) => BlocProvider(
          create: (_) => locator.get<CreateOrderCubit>(
            instanceName: 'CreateSavedOrderCubit',
          ),
          child: SetOrderPage._(
            type: SetOrderPageType.updateSavedOrder,
            order: savedOrder,
          ),
        ),
      ),
    );
  }

  @override
  State<SetOrderPage> createState() => _SetOrderPageState();
}

class _SetOrderPageState extends State<SetOrderPage> {
  DriverEntity? driver;
  List<Waypoint> waypoints = [];
  DateTime dateTime = DateTime.now().add(const Duration(minutes: 5));
  String fullName = '';
  String phone = '';
  String cash = '';
  String credit = '';
  String toll = '';
  String description = '';
  String paymentType = 'cash';
  OrganizationEntity? organization;
  bool isToll = false;
  bool important = false;
  bool female = false;

  bool get validated {
    final isWaypointsIsNotEmpty = waypoints.isNotEmpty;
    final isPhoneNotEmpty = phone.isNotEmpty;
    final isValuesNotEmpty = paymentType == 'cash' ? cash.isNotEmpty : (cash.isNotEmpty || credit.isNotEmpty);

    return isWaypointsIsNotEmpty && isPhoneNotEmpty && isValuesNotEmpty;
  }

  late CreateOrderCubit _createOrderCubit;

  @override
  void initState() {
    initFields();
    super.initState();
  }

  void initFields() {
    _createOrderCubit = BlocProvider.of<CreateOrderCubit>(context);
    if (widget.order != null) {
      driver = widget.order?.driver;
      waypoints = List.of(widget.order?.waypoints ?? []);
      fullName = widget.order?.customerName ?? '';
      phone = widget.order?.customerPhone.toString() ?? '';
      cash = widget.order?.orderValue.toString() ?? '';
      credit = widget.order?.orderCreditValue.toString() ?? '';
      toll = widget.order?.orderTollValue.toString() ?? '';
      description = widget.order?.description ?? '';
      organization = widget.order?.organization;
      paymentType = widget.order?.paymentType ?? 'cash';
      isToll = widget.order?.toll ?? false;
      important = widget.order?.important ?? false;
      female = widget.order?.female ?? false;

      switch (widget.type) {
        case SetOrderPageType.createOrder:
          dateTime = DateTime.now().add(const Duration(minutes: 5));
          break;
        case SetOrderPageType.updateOrder:
          dateTime = widget.order!.dateTime;
          break;
        case SetOrderPageType.createOrderFromSaved:
          final today = DateTime.now();
          final order = widget.order!;
          dateTime = DateTime(
            today.year,
            today.month,
            today.day,
            order.dateTime.hour,
            order.dateTime.minute,
          );
          break;
        case SetOrderPageType.updateSavedOrder:
          dateTime = widget.order!.dateTime;
          final today = DateTime.now();
          final order = widget.order!;
          dateTime = DateTime(
            today.year,
            today.month,
            today.day,
            order.dateTime.hour,
            order.dateTime.minute,
          );
          break;
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    _createOrderCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.type.toString()),
        ),
        body: Container(
          padding: const EdgeInsets.all(14),
          child: SingleChildScrollView(
            child: SafeArea(
              child: Column(
                children: [
                  ChooseDriverWidget(
                    onChange: (DriverEntity? driver) {
                      setState(() => this.driver = driver);
                    },
                    driver: driver,
                  ),
                  const SizedBox(height: 14),
                  SelectWaypointsWidget(
                    waypoints: waypoints,
                    onChanged: (List<Waypoint> waypoints) {
                      setState(() => this.waypoints = waypoints);
                    },
                  ),
                  const SizedBox(height: 14),
                  SetDateWidget(
                    dateTime: dateTime,
                    onChanged: (value) {
                      setState(() => dateTime = value);
                    },
                  ),
                  const SizedBox(height: 14),
                  CustomerDataWidget(
                    initialName: fullName,
                    initialPhone: phone,
                    onFullNameChanged: (String value) {
                      setState(() => fullName = value);
                    },
                    onPhoneChanged: (String value) {
                      setState(() => phone = value);
                    },
                  ),
                  const SizedBox(height: 14),
                  PaymentTypeWidget(
                    initialOrganization: organization,
                    initialCashValue: cash,
                    initialCreditValue: credit,
                    initialTollValue: toll,
                    onCashChanged: (String value) {
                      setState(() => cash = value);
                    },
                    onCreditChanged: (String value) {
                      setState(() => credit = value);
                    },
                    onTollChanged: (String value) {
                      setState(() => toll = value);
                    },
                    onChangedType: (
                      String value, {
                      OrganizationEntity? organization,
                    }) {
                      setState(() {
                        paymentType = value;
                        this.organization = organization;
                      });
                    },
                  ),
                  const SizedBox(height: 14),
                  Section(
                    title: "Options",
                    children: [
                      Row(
                        children: [
                          const Icon(
                            Icons.toll,
                            color: Colors.deepOrange,
                          ),
                          const SizedBox(width: 10),
                          const Text("Toll"),
                          const Spacer(),
                          SizedBox(
                            height: 20,
                            child: CupertinoSwitch(
                              value: isToll,
                              onChanged: (value) {
                                setState(() => isToll = value);
                              },
                              activeTrackColor: AppColors.background,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 15),
                      Row(
                        children: [
                          const Icon(Icons.priority_high, color: Colors.red),
                          const SizedBox(width: 10),
                          const Text("Important"),
                          const Spacer(),
                          SizedBox(
                            height: 20,
                            child: CupertinoSwitch(
                              value: important,
                              onChanged: (value) {
                                setState(() => important = !important);
                              },
                              activeColor: AppColors.background,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 15),
                      Row(
                        children: [
                          const Icon(Icons.female, color: Colors.pinkAccent),
                          const SizedBox(width: 10),
                          const Text("Female"),
                          const Spacer(),
                          SizedBox(
                            height: 20,
                            child: CupertinoSwitch(
                              value: female,
                              onChanged: (value) {
                                setState(() => female = !female);
                              },
                              activeColor: AppColors.background,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 14),
                  DescriptionWidget(
                    initialDescription: description,
                    onChanged: (String value) =>
                        setState(() => description = value),
                  ),
                  const SizedBox(height: 14),
                  BlocConsumer<CreateOrderCubit, CreateOrderState>(
                    listener: (context, state) {
                      if (state is CreateOrderFailure) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Error saving order"),
                          ),
                        );
                      }
                      if (state is CreateOrderCreated) {
                        Navigator.pop(context, true);
                        BlocProvider.of<OrdersCubit>(context).refresh();
                      }
                    },
                    builder: (context, state) {
                      return ButtonWidget(
                        onPressed: validated ? onSave : null,
                        title: widget.type.toString(),
                        loading: state is CreateOrderLoading,
                      );
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> onSave() async {
    switch (widget.type) {
      case SetOrderPageType.createOrder:
        await onCreateOrder();
        break;
      case SetOrderPageType.updateOrder:
        await onUpdateOrder();
        break;
      case SetOrderPageType.createOrderFromSaved:
        await onCreateOrder();
        break;
      case SetOrderPageType.updateSavedOrder:
        await onUpdateOrder();
        break;
    }
  }

  Future<void> onCreateOrder() async {
    final order = OrderEntity(
      id: 0,
      waypoints: waypoints,
      driver: driver,
      date: dateTime.millisecondsSinceEpoch ~/ 1000,
      status: OrderStatusEntity.all,
      customerName: fullName,
      customerPhone: phone,
      orderValue: double.tryParse(cash),
      orderCreditValue: double.tryParse(credit),
      orderTollValue: double.tryParse(toll),
      paymentType: paymentType,
      organization: organization,
      important: important,
      toll: isToll,
      description: description,
      female: female,
    );
    await _createOrderCubit.create(order);
  }

  Future<void> onUpdateOrder() async {
    final order = OrderEntity(
      id: widget.order!.id,
      waypoints: waypoints,
      driver: driver,
      date: dateTime.millisecondsSinceEpoch ~/ 1000,
      status: OrderStatusEntity.all,
      customerName: fullName,
      customerPhone: phone,
      toll: isToll,
      orderValue: double.tryParse(cash),
      orderCreditValue: double.tryParse(credit),
      orderTollValue: double.tryParse(toll),
      paymentType: paymentType,
      organization: organization,
      important: important,
      description: description,
      female: female,
    );
    await _createOrderCubit.update(order);
  }
}
