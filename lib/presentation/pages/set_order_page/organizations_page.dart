import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/organization.dart';
import '../../../service_locator.dart';
import '../../blocs/organizations/organizations_cubit.dart';
import '../../theme/app_theme.dart';
import '../../widgets/failure_widget.dart';
import '../../widgets/loader_widget.dart';
import '../../widgets/search_input.dart';

class OrganizationsBottomSheet extends StatefulWidget {
  const OrganizationsBottomSheet({super.key});

  static Future<T?> show<T>(BuildContext context) async {
    return await showModalBottomSheet(
      showDragHandle: true,
      isScrollControlled: true,
      useSafeArea: true,
      backgroundColor: AppColors.background,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(34),
      ),
      context: context,
      builder: (_) => BlocProvider(
        create: (context) => locator.get<OrganizationsCubit>(),
        child: const OrganizationsBottomSheet(),
      ),
    );
  }

  @override
  State<OrganizationsBottomSheet> createState() =>
      _OrganizationsBottomSheetState();
}

class _OrganizationsBottomSheetState extends State<OrganizationsBottomSheet> {
  final TextEditingController controller = TextEditingController();
  String? search;

  @override
  void initState() {
    BlocProvider.of<OrganizationsCubit>(context).load();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          SearchInput(
            controller: controller,
            hint: "Search organization",
            onSearchTextChanged: (value) {
              setState(() => search = value);
            },
          ),
          BlocBuilder<OrganizationsCubit, OrganizationsState>(
            builder: (context, state) {
              if (state is OrganizationsLoaded) {
                return SingleChildScrollView(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Column(
                    children: state.organizations
                        .where((element) {
                          if (search != null && search!.trim().isNotEmpty) {
                            return element.name
                                .toLowerCase()
                                .contains(search!.toLowerCase());
                          }
                          return true;
                        })
                        .map(
                          (organization) => Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: ListTile(
                              title: Text(organization.name),
                              iconColor: AppColors.background,
                              leading: const Icon(Icons.business),
                              tileColor: Colors.white,
                              minLeadingWidth: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              onTap: () => Navigator.pop<OrganizationEntity>(
                                context,
                                organization,
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                );
              }

              if (state is OrganizationsFailure) {
                return FailureWidget(
                  message: "Error getting organizations",
                  onPressed: () {
                    BlocProvider.of<OrganizationsCubit>(context).load();
                  },
                );
              }

              return const Center(
                child: LoaderWidget(),
              );
            },
          ),
        ],
      ),
    );
  }
}
