import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:ui';

import 'package:admin/domain/entities/driver.dart';
import 'package:admin/presentation/blocs/drivers/drivers_cubit.dart';
import 'package:admin/presentation/pages/drivers_page/driver_detail_page.dart';
import 'package:admin/presentation/theme/app_theme.dart';
import 'package:admin/presentation/widgets/loader_widget.dart';
import 'package:admin/presentation/widgets/select_driver_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'widgets/location_painter.dart';

class GPSPage extends StatefulWidget {
  const GPSPage({super.key});

  @override
  State<GPSPage> createState() => _GPSPageState();
}

class _GPSPageState extends State<GPSPage> {
  late GoogleMapController mapController;
  final LatLng cameraPosition = const LatLng(
    40.6974881,
    -73.979681,
  );
  Set<Marker> markers = {};

  @override
  void initState() {
    super.initState();
    BlocProvider.of<DriversCubit>(context).getAll();
    BlocProvider.of<DriversCubit>(context).subscribeUpdates();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final Position position = await _determinePosition();
      mapController.animateCamera(
        CameraUpdate.newLatLng(
          LatLng(position.latitude, position.longitude),
        ),
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
    mapController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        BlocListener<DriversCubit, DriversState>(
          listener: _driversListener,
          child: SafeArea(
            child: GoogleMap(
              myLocationButtonEnabled: false,
              myLocationEnabled: false,
              initialCameraPosition: CameraPosition(
                target: cameraPosition,
                zoom: 15.0,
              ),
              onMapCreated: (GoogleMapController controller) {
                mapController = controller;
              },
              markers: markers,
            ),
          ),
        ),
        Positioned(
          top: 10,
          right: 10,
          child: FloatingActionButton(
            backgroundColor: Colors.white,
            onPressed: () async {
              final driver =
                  await SelectDriverBottomSheet.show<DriverEntity>(context);

              if (driver?.latitude != null && driver?.longitude != null) {
                var latLong = LatLng(
                  driver?.latitude ?? 0,
                  driver?.longitude ?? 0,
                );

                mapController.moveCamera(CameraUpdate.newLatLng(latLong));
                setState(() {});
              }
            },
            child: Icon(
              Icons.location_history,
              color: AppColors.background,
              size: 30,
            ),
          ),
        ),
        BlocBuilder<DriversCubit, DriversState>(
          builder: (context, state) {
            if (state is DriversLoading) {
              return Align(
                alignment: Alignment.center,
                child: Container(
                  padding: const EdgeInsets.all(30),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.8),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: LoaderWidget(color: AppColors.background),
                ),
              );
            }
            return const SizedBox();
          },
        ),
      ],
    );
  }

  Future<Marker> makeMarker(
    LatLng position,
    String letter,
    Color color,
    DriverEntity driver,
  ) async {
    final Uint8List markerIcon = await createMarkerIcon(letter, color);
    return Marker(
      markerId: MarkerId(position.toString()),
      position: position,
      icon: BitmapDescriptor.fromBytes(markerIcon),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) {
              return DriverDetailPage(driver: driver);
            },
          ),
        );
      },
    );
  }

  Future<Uint8List> createMarkerIcon(String letter, Color color) async {
    const width = 100.0;
    final size = Size(width, (width * 1.3214285714285714).toDouble());
    final PictureRecorder recorder = PictureRecorder();
    final Canvas canvas = Canvas(recorder);

    final LocationPainter svgPainter = LocationPainter(
      letter: letter,
      circleColor: color,
    );

    svgPainter.paint(canvas, size);
    final img = await recorder
        .endRecording()
        .toImage(size.width.toInt(), size.height.toInt());
    final data = await img.toByteData(format: ui.ImageByteFormat.png);
    return data!.buffer.asUint8List();
  }

  void _driversListener(BuildContext context, DriversState state) async {
    if (state is DriversLoaded) {
      markers.clear();
      var drivers = state.drivers
          .where((d) => d.longitude != null && d.latitude != null)
          .toList();

      debugPrint("Drivers: ${drivers.length}");

      for (var driver in drivers) {
        if (driver.latitude != null && driver.longitude != null) {
          var marker = await makeMarker(
            LatLng(driver.latitude ?? 0, driver.longitude ?? 0),
            driver.name.substring(0, 1),
            Colors.accents[1].shade200,
            driver,
          );
          markers.add(marker);
        }
      }

      setState(() => markers);
    }
  }
}

/// Determine the current position of the device.
///
/// When the location services are not enabled or permissions
/// are denied the `Future` will return an error.
Future<Position> _determinePosition() async {
  bool serviceEnabled;
  LocationPermission permission;

  // Test if location services are enabled.
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the
    // App to enable the location services.
    return Future.error('Location services are disabled.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    // Permissions are denied forever, handle appropriately.
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }

  // When we reach here, permissions are granted and we can
  // continue accessing the position of the device.
  return await Geolocator.getCurrentPosition();
}
