import 'package:flutter/material.dart';

class LocationPainter extends CustomPainter {
  final String letter;
  final Color circleColor;
  final Color strokeColor;

  LocationPainter({
    required this.letter,
    this.circleColor = const Color(0xFFABDAB8),
    this.strokeColor = const Color(0xFF0C8CE9),
  });

  @override
  void paint(Canvas canvas, Size size) {
    final path = Path();
    path.moveTo(size.width * 0.5, size.height * 0.9189189);
    path.lineTo(size.width * 0.4785714, size.height * 0.9405405);
    path.lineTo(size.width * 0.4994, size.height * 0.9523622);
    path.lineTo(size.width * 0.5206429, size.height * 0.940973);
    path.lineTo(size.width * 0.5, size.height * 0.9189189);
    path.close();
    path.moveTo(size.width * 0.5, size.height * 0.9189189);
    path.cubicTo(
        size.width * 0.5206429,
        size.height * 0.940973,
        size.width * 0.5206536,
        size.height * 0.9409676,
        size.width * 0.5206679,
        size.height * 0.9409622);
    path.lineTo(size.width * 0.5207, size.height * 0.9409432);
    path.lineTo(size.width * 0.5207929, size.height * 0.9408919);
    path.lineTo(size.width * 0.5211, size.height * 0.940727);
    path.lineTo(size.width * 0.5221679, size.height * 0.9401432);
    path.cubicTo(
        size.width * 0.5230821,
        size.height * 0.9396459,
        size.width * 0.5243893,
        size.height * 0.9389243,
        size.width * 0.5260643,
        size.height * 0.9379865);
    path.cubicTo(
        size.width * 0.5294107,
        size.height * 0.9361081,
        size.width * 0.5342357,
        size.height * 0.9333649,
        size.width * 0.540325,
        size.height * 0.9297838);
    path.cubicTo(
        size.width * 0.5524964,
        size.height * 0.9226297,
        size.width * 0.5697464,
        size.height * 0.9121297,
        size.width * 0.590375,
        size.height * 0.8985784);
    path.cubicTo(
        size.width * 0.6315929,
        size.height * 0.8715027,
        size.width * 0.6865571,
        size.height * 0.8320784,
        size.width * 0.7416036,
        size.height * 0.7826108);
    path.cubicTo(
        size.width * 0.8510286,
        size.height * 0.6842784,
        size.width * 0.9642857,
        size.height * 0.5430649,
        size.width * 0.9642857,
        size.height * 0.3783784);
    path.cubicTo(
        size.width * 0.9642857,
        size.height * 0.1843324,
        size.width * 0.7564179,
        size.height * 0.02702703,
        size.width * 0.5,
        size.height * 0.02702703);
    path.cubicTo(
        size.width * 0.2435821,
        size.height * 0.02702703,
        size.width * 0.03571429,
        size.height * 0.1843324,
        size.width * 0.03571429,
        size.height * 0.3783784);
    path.cubicTo(
        size.width * 0.03571429,
        size.height * 0.5364486,
        size.width * 0.1491886,
        size.height * 0.6776838,
        size.width * 0.2582004,
        size.height * 0.7773676);
    path.cubicTo(
        size.width * 0.3131346,
        size.height * 0.8276,
        size.width * 0.3679857,
        size.height * 0.8682378,
        size.width * 0.4091036,
        size.height * 0.896327);
    path.cubicTo(
        size.width * 0.4296857,
        size.height * 0.9103892,
        size.width * 0.4468857,
        size.height * 0.9213459,
        size.width * 0.4590143,
        size.height * 0.9288324);
    path.cubicTo(
        size.width * 0.4650821,
        size.height * 0.9325757,
        size.width * 0.4698821,
        size.height * 0.9354541,
        size.width * 0.4732107,
        size.height * 0.9374189);
    path.cubicTo(
        size.width * 0.474875,
        size.height * 0.9384027,
        size.width * 0.4761714,
        size.height * 0.9391595,
        size.width * 0.477075,
        size.height * 0.9396811);
    path.lineTo(size.width * 0.4781286, size.height * 0.9402865);
    path.lineTo(size.width * 0.4784286, size.height * 0.9404595);
    path.lineTo(size.width * 0.4785179, size.height * 0.9405108);
    path.lineTo(size.width * 0.47855, size.height * 0.9405297);
    path.cubicTo(
        size.width * 0.4785607,
        size.height * 0.9405351,
        size.width * 0.4785714,
        size.height * 0.9405405,
        size.width * 0.5,
        size.height * 0.9189189);
    path.close();

    // Fill
    final fillPaint = Paint()..color = strokeColor;
    canvas.drawPath(path, fillPaint);

    // Stroke
    final strokePaint = Paint()
      ..color = strokeColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.07142857;
    canvas.drawPath(path, strokePaint);

    // Circle
    final circlePaint = Paint()
      ..color = circleColor
      ..style = PaintingStyle.fill;
    canvas.drawCircle(
      Offset(size.width * 0.5, size.height * 0.3783784),
      size.width * 0.4642857,
      circlePaint,
    );

    // Text
    final textSpan = TextSpan(
      text: letter,
      style: TextStyle(
        fontSize: size.height / 2,
        fontWeight: FontWeight.w500,
        color: Colors.white,
      ),
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout();
    textPainter.paint(
      canvas,
      Offset(
        size.width * 0.4642857 - size.height / 6.5,
        size.height * 0.3783784 - size.height / 3.5,
      ),
    );
  }

  @override
  bool shouldRepaint(LocationPainter oldDelegate) => false;
}