import 'package:admin/presentation/blocs/sign_in/sign_in_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:google_fonts/google_fonts.dart';

import '../widgets/button_widget.dart';
import '../widgets/text_input.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final TextEditingController loginController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: KeyboardDismissOnTap(
          dismissOnCapturedTaps: true,
          child: SafeArea(
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(horizontal: 36),
              child: Column(
                children: [
                  const Spacer(),
                  const SizedBox(height: 22),
                  Text(
                    "Sign In",
                    style: GoogleFonts.montserrat(
                      fontSize: 36,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  const SizedBox(height: 20),
                  Text(
                    "Please enter the Login & Password for your User or Admin account!",
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      height: 19 / 16,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const Spacer(
                    flex: 2,
                  ),
                  TextInput(
                    controller: loginController,
                    hint: "Login",
                  ),
                  const SizedBox(height: 20),
                  TextInput(
                    controller: passwordController,
                    hint: "Password",
                    obscure: true,
                  ),
                  const Spacer(
                    flex: 2,
                  ),
                  BlocBuilder<SignInCubit, SignInState>(
                    builder: (context, state) {
                      return ButtonWidget(
                        title: "Sign in",
                        onPressed: () {
                          BlocProvider.of<SignInCubit>(context).signIn(
                            email: loginController.text,
                            password: passwordController.text,
                          );
                        },
                        loading: state is SignInLoading,
                      );
                    },
                  ),
                  const Spacer(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
