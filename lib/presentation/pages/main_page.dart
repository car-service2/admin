import 'package:admin/domain/entities/order.dart';
import 'package:admin/presentation/blocs/addresses/addresses_cubit.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter_cubit.dart';
import 'package:admin/presentation/blocs/drivers/drivers_cubit.dart';
import 'package:admin/presentation/blocs/orders/orders_cubit.dart';
import 'package:admin/presentation/pages/orders_page/saved_orders_page.dart';
import 'package:admin/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

import '../blocs/menu/menu_cubit.dart';
import '../widgets/bottom_nav_bar.dart';
import 'drivers_page/drivers_page.dart';
import 'gps_page/gps_page.dart';
import 'orders_page/orders_page.dart';
import 'set_order_page/set_order_page.dart';
import 'statistics_page/statistics_page.dart';

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      dismissOnCapturedTaps: true,
      child: Scaffold(
        extendBody: true,
        appBar: AppBar(
          title: BlocBuilder<MenuCubit, Menu>(
            builder: (context, state) {
              return Text(state.toString());
            },
          ),
          actions: [
            BlocBuilder<MenuCubit, Menu>(
              builder: (context, state) {
                if (state == Menu.drivers || state == Menu.orders) {
                  return IconButton(
                    onPressed: () {
                      if (state == Menu.drivers) {
                        BlocProvider.of<DriverFilterCubit>(context)
                            .setFilter(DriverFilter.initial());
                        BlocProvider.of<AddressesCubit>(context);
                      }
                      if (state == Menu.orders) {
                        BlocProvider.of<OrdersCubit>(context).refresh(
                          statuses: [
                            OrderStatusEntity.saved,
                            OrderStatusEntity.pending,
                            OrderStatusEntity.canceled,
                          ],
                          orderSort: OrderSortEntity.asc
                        );
                      }
                    },
                    icon: const Icon(Icons.refresh),
                  );
                }

                return const SizedBox();
              },
            ),
            BlocBuilder<MenuCubit, Menu>(
              builder: (context, state) {
                if (state == Menu.orders) {
                  return IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const SavedOrdersPage(),
                        ),
                      );
                    },
                    icon: const Icon(Icons.format_list_bulleted),
                  );
                }

                return const SizedBox();
              },
            ),
          ],
        ),
        body: BlocBuilder<MenuCubit, Menu>(
          builder: (BuildContext context, menu) {
            return switch (menu) {
              Menu.drivers => const DriversPage(),
              Menu.orders => const OrdersPage(),
              Menu.gps => BlocProvider(
                  create: (context) => locator.get<DriversCubit>(),
                  child: const GPSPage(),
                ),
              Menu.statistics => const StatisticsPage(),
            };
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.black,
          elevation: 0,
          onPressed: () => SetOrderPage.createOrder(context),
          child: const Icon(Icons.add),
        ),
        bottomNavigationBar: const BottomNavBar(),
      ),
    );
  }
}
