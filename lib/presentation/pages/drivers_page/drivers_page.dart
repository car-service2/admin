import 'dart:async';

import 'package:admin/domain/entities/address.dart';
import 'package:admin/presentation/blocs/addresses/addresses_cubit.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter_cubit.dart';
import 'package:admin/presentation/pages/drivers_page/driver_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../domain/entities/driver.dart';
import '../../blocs/drivers/drivers_cubit.dart';
import '../../widgets/search_input.dart';
import 'widgets/address_list.dart';
import 'widgets/driver_list.dart';
import 'widgets/filter_bottom_sheet.dart';

class DriversPage extends StatefulWidget {
  const DriversPage({super.key});

  @override
  State<DriversPage> createState() => _DriversPageState();
}

class _DriversPageState extends State<DriversPage> {
  final _searchController = TextEditingController();
  Timer? _debounceTimer;

  @override
  void dispose() {
    _searchController.dispose();
    _debounceTimer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _searchController.text = BlocProvider.of<DriversCubit>(context).search;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          /// search and filter
          SizedBox(
            height: 52,
            child: Row(
              children: [
                Expanded(
                  child: SearchInput(
                    controller: _searchController,
                    hint: "Search",
                    onSearchTextChanged: _onSearchTextChanged,
                  ),
                ),
                const SizedBox(width: 10),
                BlocBuilder<DriverFilterCubit, DriverFilter>(
                  builder: (context, filter) {
                    Map<String, dynamic> filters = filter.toQueryParam();
                    return GestureDetector(
                      onTap: () {
                        FilterBottomSheet.show(context, filters);
                      },
                      child: Container(
                        padding: const EdgeInsets.all(14),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color:
                              filter.isApplied ? Colors.yellow : Colors.white,
                        ),
                        child: SvgPicture.asset("assets/icons/filter.svg"),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),

          /// address list with drivers count
          SizedBox.fromSize(
            size: const Size.fromHeight(50),
            child: BlocBuilder<AddressesCubit, AddressesState>(
              bloc: BlocProvider.of<AddressesCubit>(context)..load(),
              builder: (context, state) {
                List<AddressEntity> addresses = [];

                if (state is AddressesLoaded) {
                  addresses = state.addresses;
                }

                return BlocBuilder<DriverFilterCubit, DriverFilter>(
                  builder: (context, state) {
                    return AddressList(
                      addresses: addresses,
                      selectedId: state.addressId,
                      onSelected: (AddressEntity selectedAddress) {
                        BlocProvider.of<DriverFilterCubit>(context)
                            .setAddress(selectedAddress.id);
                      },
                    );
                  },
                );
              },
            ),
          ),
          const SizedBox(height: 3),

          /// driver list
          DriversList(
            onSelectDriver: (DriverEntity driver) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) {
                    return DriverDetailPage(driver: driver);
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  void _onSearchTextChanged(String text) {
    _debounceTimer?.cancel();

    // Создаем новый таймер, чтобы вызвать обработчик через 500 миллисекунд
    _debounceTimer = Timer(const Duration(milliseconds: 500), () {
      // Здесь можно выполнить ваш поиск или другие действия с текстом
      debugPrint("Выполняем поиск $text");
      BlocProvider.of<DriverFilterCubit>(context).setSearch(text);
    });
  }
}
