import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../domain/entities/driver.dart';

class DriverListTile extends StatelessWidget {
  final VoidCallback onTap;
  final DriverEntity driver;

  const DriverListTile({
    super.key,
    required this.driver,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 6),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: driver.blocked ? Colors.red.shade100 : Colors.white,
          borderRadius: BorderRadius.circular(22),
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(14),
              child: Image.network(
                driver.image,
                width: 52,
                height: 52,
                fit: BoxFit.cover,
                errorBuilder: (_, __, ___) {
                  return Container(
                    height: 52,
                    width: 52,
                    color: Colors.grey.shade200,
                    child: const Icon(Icons.person),
                  );
                },
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    driver.name,
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(height: 2),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SvgPicture.asset("assets/icons/car-line.svg"),
                      const SizedBox(width: 4),
                      Flexible(
                        child: Text(
                          driver.carInfo,
                          style: GoogleFonts.montserrat(
                              fontSize: 12,
                              color: const Color.fromRGBO(13, 13, 13, 0.50)),
                        ),
                      ),
                      const SizedBox(width: 12),
                      SvgPicture.asset("assets/icons/user-line.svg"),
                      const SizedBox(width: 4),
                      Flexible(
                        child: Text(
                          driver.gender,
                          style: GoogleFonts.montserrat(
                              fontSize: 12,
                              color: const Color.fromRGBO(13, 13, 13, 0.50)),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 2),
                  Text(
                    driver.waiting,
                    style: GoogleFonts.montserrat(
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Text(
                  "ID: ${driver.id}",
                  style: GoogleFonts.montserrat(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: const Color(0xFF0D0D0D),
                  ),
                ),
                const SizedBox(height: 9),
                Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 8,
                    vertical: 5,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: driver.inLine
                        ? const Color(0xFF00D38C)
                        : Colors.red.shade400,
                  ),
                  child: Text(
                    driver.inLine ? "Online" : "Offline",
                    style: GoogleFonts.montserrat(
                      color: Colors.white,
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
