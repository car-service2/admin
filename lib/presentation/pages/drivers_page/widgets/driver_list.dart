import 'package:admin/domain/entities/driver.dart';
import 'package:admin/presentation/blocs/addresses/addresses_cubit.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/drivers/drivers_cubit.dart';
import '../../../widgets/failure_widget.dart';
import '../../../widgets/loader_widget.dart';
import 'driver_list_tile.dart';

class DriversList extends StatefulWidget {
  final void Function(DriverEntity driver) onSelectDriver;

  const DriversList({
    super.key,
    required this.onSelectDriver,
  });

  @override
  State<DriversList> createState() => _DriversListState();
}

class _DriversListState extends State<DriversList> {
  final ScrollController _scrollController = ScrollController();
  late final DriversCubit _driversCubit;
  late final AddressesCubit _addressesCubit;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _driversCubit = BlocProvider.of<DriversCubit>(context);
    _addressesCubit = BlocProvider.of<AddressesCubit>(context);
    _driversCubit.refresh();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScroll);
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.atEdge &&
        _scrollController.position.pixels != 0) {
      // Достигнут конец списка
      _loadData();
    }
  }

  Future<void> _loadData() async {
    if (!_isLoading) {
      // Предотвращаем повторную загрузку данных во время подгрузки
      setState(() => _isLoading = true);

      try {
        await _driversCubit.getDrivers(); // Загружаем данные из кубита
      } catch (e) {
        debugPrint(e.toString());
      }

      // Завершаем подгрузку
      setState(() => _isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DriversCubit, DriversState>(
      builder: (context, state) {
        if (state is DriversLoaded) {
          return Flexible(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(34),
              child: Scrollbar(
                controller: _scrollController,
                child: RefreshIndicator.adaptive(
                  onRefresh: _onRefresh,
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 5,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(34),
                      color: const Color(0xFFF6F6F6),
                    ),
                    child: NotificationListener<ScrollNotification>(
                      onNotification: (notification) {
                        // Добавляем обработчик прокрутки для определения конца списка
                        if (!_isLoading &&
                            notification.metrics.pixels >=
                                notification.metrics.maxScrollExtent) {
                          _loadData();
                        }
                        return false;
                      },
                      child: ListView.builder(
                        physics: const AlwaysScrollableScrollPhysics(),
                        controller: _scrollController,
                        itemCount: _isLoading
                            ? state.drivers.length + 1
                            : state.drivers.length,
                        itemBuilder: (context, index) {
                          if (index < state.drivers.length) {
                            // Отображаем элемент списка
                            final driver = state.drivers[index];
                            return DriverListTile(
                              driver: driver,
                              onTap: () {
                                widget.onSelectDriver(driver);
                              },
                            );
                          } else {
                            // Достигли конца списка, показываем индикатор загрузки
                            return _isLoading
                                ? const Center(
                                    child: SizedBox.square(
                                      dimension: 25,
                                      child: CircularProgressIndicator(
                                        color: Colors.green,
                                        strokeWidth: 1.5,
                                      ),
                                    ),
                                  )
                                : Container();
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        } else if (state is DriversFailure) {
          return Center(
            child: FailureWidget(
              message: state.message,
              onPressed: () {
                _driversCubit.refresh();
                _addressesCubit.load();
              },
            ),
          );
        }

        return const LoaderWidget();
      },
    );
  }

  Future<void> _onRefresh() async {
    // monitor network fetch
    BlocProvider.of<DriverFilterCubit>(context).setFilter(DriverFilter.initial());
    // await _driversCubit.refresh();
    await _addressesCubit.load();
    // if failed,use refreshFailed()
  }
}
