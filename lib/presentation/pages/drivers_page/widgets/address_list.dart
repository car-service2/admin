import 'package:admin/domain/entities/address.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class AddressList extends StatelessWidget {
  final List<AddressEntity> addresses;
  final int? selectedId;

  final Function(AddressEntity selected) onSelected;

  const AddressList({
    super.key,
    required this.addresses,
    required this.selectedId,
    required this.onSelected,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      clipBehavior: Clip.none,
      child: Row(
        children: addresses.isEmpty
            ? List.generate(
                4,
                (index) => Shimmer.fromColors(
                  baseColor: Colors.white.withOpacity(0.5),
                  highlightColor: const Color(0xFF76E2B3),
                  child: AddressItem(
                    title: "Address (5)",
                    onPressed: () {},
                  ),
                ),
              )
            : addresses
                .map(
                  (address) => AddressItem(
                    selected: address.id == selectedId,
                    title: "${address.name} (${address.driversCount ?? 0})",
                    onPressed: () => onSelected(address),
                  ),
                )
                .toList(),
      ),
    );
  }
}

class AddressItem extends StatelessWidget {
  final String title;
  final VoidCallback onPressed;
  final bool selected;

  const AddressItem({
    super.key,
    required this.title,
    required this.onPressed,
    this.selected = false,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        margin: const EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
          color: selected ? Colors.yellow : Colors.white,
          borderRadius: BorderRadius.circular(50),
        ),
        padding: const EdgeInsets.all(10),
        child: Text(
          title,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}
