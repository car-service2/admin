import 'package:admin/presentation/blocs/driver_filter/driver_filter.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../blocs/addresses/addresses_cubit.dart';

class FilterBottomSheet extends StatefulWidget {
  final Map<String, dynamic> filters;

  const FilterBottomSheet({
    super.key,
    required this.filters,
  });

  static Future<T?> show<T>(
    context,
    Map<String, dynamic> filters,
  ) {
    return showModalBottomSheet<T>(
      backgroundColor: Colors.white,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      showDragHandle: true,
      context: context,
      builder: (_) {
        return BlocProvider.value(
          value: BlocProvider.of<DriverFilterCubit>(context),
          child: FilterBottomSheet(filters: filters),
        );
      },
    );
  }

  @override
  State<FilterBottomSheet> createState() => _FilterBottomSheetState();
}

class _FilterBottomSheetState extends State<FilterBottomSheet> {
  bool inline = false;
  bool offline = false;
  int? selectedAddress;
  bool male = false;
  bool female = false;

  @override
  void initState() {
    setInitialFilters();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const HeaderFilter(title: "Status"),
            const SizedBox(height: 16),
            FilterCheckBox(
              title: 'Online',
              value: inline,
              onChanged: (bool value) {
                setState(() {
                  inline = value;
                  offline = false;
                });
              },
            ),
            FilterCheckBox(
              title: 'Offline',
              value: offline,
              onChanged: (bool value) {
                setState(() {
                  offline = value;
                  inline = false;
                });
              },
            ),
            const Divider(color: Color.fromRGBO(13, 13, 13, 0.10)),
            const SizedBox(height: 8),
            const HeaderFilter(title: "Location"),
            const SizedBox(height: 16),
            BlocBuilder<AddressesCubit, AddressesState>(
              builder: (_, state) {
                if (state is AddressesLoaded) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: state.addresses
                        .map(
                          (e) => FilterCheckBox(
                            title: "${e.name} (${e.driversCount ?? "0"})",
                            value: selectedAddress == e.id,
                            onChanged: (bool value) {
                              if (value == false) {
                                setState(() => selectedAddress = null);
                              } else {
                                setState(() => selectedAddress = e.id);
                              }
                            },
                          ),
                        )
                        .toList(),
                  );
                }
                return const SizedBox();
              },
            ),
            const Divider(color: Color.fromRGBO(13, 13, 13, 0.10)),
            const SizedBox(height: 8),
            const HeaderFilter(title: "Gender"),
            const SizedBox(height: 16),
            FilterCheckBox(
              title: 'Male',
              value: male,
              onChanged: (bool value) {
                setState(() {
                  male = value;
                  female = false;
                });
              },
            ),
            FilterCheckBox(
              title: 'Female',
              value: female,
              onChanged: (bool value) {
                setState(() {
                  female = value;
                  male = false;
                });
              },
            ),
            const Divider(color: Color.fromRGBO(13, 13, 13, 0.10)),
            const SizedBox(height: 16),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 0,
                minimumSize: const Size.fromHeight(52),
                maximumSize: const Size.fromHeight(52),
              ),
              onPressed: () {
                applyFilter();
                Navigator.pop(context);
              },
              child: const Text("Filter"),
            )
          ],
        ),
      ),
    );
  }

  void applyFilter() {
    bool? isOnline;
    bool? isMale;

    if (inline) {
      isOnline = true;
    }
    if (offline) {
      isOnline = false;
    }
    if (male) {
      isMale = true;
    }

    if (female) {
      isMale = false;
    }

    BlocProvider.of<DriverFilterCubit>(context).setFilter(DriverFilter(
      search: null,
      isOnline: isOnline,
      isMale: isMale,
      addressId: selectedAddress,
    ));
  }

  void setInitialFilters() {
    if (widget.filters['in_line'] != null) {
      inline = widget.filters['in_line'] == 'true';
      offline = widget.filters['in_line'] == 'false';
    }
    if (widget.filters['gender'] != null) {
      male = widget.filters['gender'] == 'male';
      female = widget.filters['gender'] == 'female';
    }
    if (widget.filters['stop_address_id'] != null) {
      selectedAddress = widget.filters['stop_address_id'];
    }
  }
}

class HeaderFilter extends StatelessWidget {
  final String title;

  const HeaderFilter({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          title,
          style: GoogleFonts.montserrat(
            fontSize: 16,
            color: const Color.fromRGBO(13, 13, 13, 0.60),
            fontWeight: FontWeight.w500,
          ),
        ),
        const Spacer(),
        SvgPicture.asset("assets/icons/arrow-down-s-line.svg"),
      ],
    );
  }
}

class FilterCheckBox extends StatelessWidget {
  final String title;
  final bool value;
  final Function(bool value) onChanged;

  const FilterCheckBox({
    super.key,
    required this.title,
    required this.value,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onChanged(!value),
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Row(
          children: [
            Expanded(
              child: Text(
                title,
                style: GoogleFonts.montserrat(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Icon(
              value
                  ? Icons.check_box_rounded
                  : Icons.check_box_outline_blank_outlined,
              color: value ? const Color(0xFF00A19A) : Colors.grey.shade400,
            )
          ],
        ),
      ),
    );
  }
}
