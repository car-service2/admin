import 'package:admin/domain/entities/driver.dart';
import 'package:admin/domain/usecases/orders_use_case.dart';
import 'package:admin/presentation/blocs/drivers/drivers_cubit.dart';
import 'package:admin/presentation/blocs/orders/orders_cubit.dart';
import 'package:admin/presentation/pages/orders_page/widgets/order_list.dart';
import 'package:admin/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class DriverDetailPage extends StatelessWidget {
  final DriverEntity driver;

  const DriverDetailPage({
    super.key,
    required this.driver,
  });

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("About driver"),
          bottom: const TabBar(tabs: [
            Tab(text: "Driver"),
            Tab(text: "Current orders"),
          ]),
        ),
        body: TabBarView(children: [
          _DriverDetail(driver: driver),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: BlocProvider<OrdersCubit>(
              create: (context) {
                return DriverCurrentOrdersCubit(
                  locator.get<OrdersUseCase>(),
                  driver,
                );
              },
              child: const OrderList(),
            ),
          ),
        ]),
      ),
    );
  }
}

class _DriverDetail extends StatefulWidget {
  final DriverEntity driver;

  const _DriverDetail({required this.driver});

  @override
  State<_DriverDetail> createState() => _DriverDetailState();
}

class _DriverDetailState extends State<_DriverDetail> {
  bool blocked = false;

  @override
  void initState() {
    blocked = widget.driver.blocked;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(22),
              color: Colors.white,
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: Image.network(
                        widget.driver.image,
                        width: 52,
                        height: 52,
                        fit: BoxFit.cover,
                        errorBuilder: (_, __, ___) {
                          return Container(
                            height: 52,
                            width: 52,
                            color: Colors.grey.shade200,
                            child: const Icon(Icons.person),
                          );
                        },
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        widget.driver.name,
                        style: GoogleFonts.montserrat(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: widget.driver.inLine
                            ? const Color(0xFF00D38C)
                            : Colors.red.shade400,
                      ),
                      child: Text(
                        widget.driver.inLine ? "Online" : "Offline",
                        style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  children: [
                    Text(
                      "ID ${widget.driver.id}",
                      style: GoogleFonts.montserrat(
                        color: const Color.fromRGBO(13, 13, 13, 0.60),
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const Spacer(),
                    Text(
                      "Driver since:",
                      style: GoogleFonts.montserrat(
                        color: const Color.fromRGBO(13, 13, 13, 0.60),
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(width: 10),
                    Text(
                      widget.driver.since ?? '',
                      style: GoogleFonts.montserrat(
                        color: const Color(0xFF0D0D0D),
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                BlocBuilder<DriversCubit, DriversState>(
                  builder: (context, state) {
                    DriverEntity driver = widget.driver;
                    if (state is DriversLoaded) {
                      driver =
                          state.drivers.firstWhere((e) => e.id == driver.id);
                    }

                    return Row(
                      children: [
                        Text(
                          "Status: ",
                          style: GoogleFonts.montserrat(
                            color: const Color.fromRGBO(13, 13, 13, 0.60),
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(width: 10),
                        Text(
                          blocked ? "Blocked" : "Active",
                          style: GoogleFonts.montserrat(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: blocked ? Colors.red.shade300 : Colors.green,
                          ),
                        ),
                        const Spacer(),
                        TextButton.icon(
                          style: ElevatedButton.styleFrom(
                            elevation: 0.0,
                            foregroundColor: Colors.black,
                            textStyle: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          onPressed: state is DriversLoaded
                              ? () async {
                                  blocked = await context
                                      .read<DriversCubit>()
                                      .block(driver.id);
                                  setState(() {});
                                }
                              : null,
                          label: Text(blocked ? "Unblock" : "Block"),
                          icon: Icon(
                            blocked ? Icons.lock_open : Icons.lock,
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 14),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(22), color: Colors.white),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Driver data",
                  style: GoogleFonts.montserrat(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 13, vertical: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: const Color(0xFFF6F6F6),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SvgPicture.asset(
                        "assets/icons/user-line.svg",
                        width: 20,
                        height: 20,
                      ),
                      const SizedBox(width: 20),
                      Text(
                        widget.driver.name,
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 13, vertical: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: const Color(0xFFF6F6F6),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SvgPicture.asset(
                        "assets/icons/noun-male-female-3118863 1.svg",
                        width: 20,
                        height: 20,
                      ),
                      const SizedBox(width: 20),
                      Text(
                        widget.driver.gender,
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 13, vertical: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: const Color(0xFFF6F6F6),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SvgPicture.asset(
                        "assets/icons/passport-line.svg",
                        width: 20,
                        height: 20,
                      ),
                      const SizedBox(width: 20),
                      Text(
                        "ID: ${widget.driver.id}",
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 13, vertical: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: const Color(0xFFF6F6F6),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SvgPicture.asset(
                        "assets/icons/car-line.svg",
                        width: 20,
                        height: 20,
                      ),
                      const SizedBox(width: 20),
                      Text(
                        widget.driver.carInfo,
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                GestureDetector(
                  onTap: () async {
                    final telUrl = Uri.parse("tel:${widget.driver.phone}");
                    if (await canLaunchUrl(telUrl)) {
                      await launchUrl(telUrl);
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 13, vertical: 16),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: const Color(0xFFF6F6F6),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SvgPicture.asset(
                          "assets/icons/phone-line.svg",
                          width: 20,
                          height: 20,
                        ),
                        const SizedBox(width: 20),
                        Text(
                          "${widget.driver.phone}",
                          style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
