import 'package:admin/domain/entities/order.dart';
import 'package:admin/presentation/blocs/orders/orders_cubit.dart';
import 'package:admin/presentation/pages/drivers_page/widgets/filter_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class OrdersFilterSheet extends StatefulWidget {
  final List<OrderStatusEntity> selectedStatuses;
  final OrderSortEntity orderSort;

  const OrdersFilterSheet({
    super.key,
    required this.selectedStatuses,
    required this.orderSort,
  });

  static Future<T?> show<T>(
    context, {
    required List<OrderStatusEntity> filters,
    required OrderSortEntity orderSort,
  }) {
    return showModalBottomSheet<T>(
      backgroundColor: Colors.white,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      showDragHandle: true,
      context: context,
      builder: (_) => OrdersFilterSheet(
        selectedStatuses: filters,
        orderSort: orderSort,
      ),
    );
  }

  @override
  State<OrdersFilterSheet> createState() => _OrdersFilterSheetState();
}

class _OrdersFilterSheetState extends State<OrdersFilterSheet> {
  List<OrderStatusEntity> selectedStatuses = [];
  OrderSortEntity sort = OrderSortEntity.asc;

  @override
  void initState() {
    selectedStatuses = widget.selectedStatuses;
    sort = widget.orderSort;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const HeaderFilter(title: "Sort by date"),
            const SizedBox(height: 16),
            GestureDetector(
              onTap: () {
                sort = OrderSortEntity.asc;
                setState(() {});
              },
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  children: [
                    const Icon(Icons.arrow_upward),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        'Ascending',
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    Icon(
                      sort == OrderSortEntity.asc
                          ? Icons.check_box_rounded
                          : Icons.check_box_outline_blank_outlined,
                      color: sort == OrderSortEntity.asc
                          ? const Color(0xFF00A19A)
                          : Colors.grey.shade400,
                    )
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                sort = OrderSortEntity.desc;
                setState(() {});
              },
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  children: [
                    const Icon(Icons.arrow_downward),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        'Descending',
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    Icon(
                      sort == OrderSortEntity.desc
                          ? Icons.check_box_rounded
                          : Icons.check_box_outline_blank_outlined,
                      color: sort == OrderSortEntity.desc
                          ? const Color(0xFF00A19A)
                          : Colors.grey.shade400,
                    )
                  ],
                ),
              ),
            ),
            const Divider(),
            const HeaderFilter(title: "Status"),
            const SizedBox(height: 16),
            ...OrderStatusEntity.values
                .map(
                  (status) => GestureDetector(
                    onTap: () {
                      if (OrderStatusEntity.all == status) {
                        selectedStatuses.clear();
                      } else {
                        selectedStatuses
                            .removeWhere((e) => e == OrderStatusEntity.all);
                      }
                      if (selectedStatuses.contains(status)) {
                        selectedStatuses.remove(status);
                      } else {
                        selectedStatuses.add(status);
                      }
                      setState(() {});
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              status.toString(),
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          Icon(
                            selectedStatuses.contains(status)
                                ? Icons.check_box_rounded
                                : Icons.check_box_outline_blank_outlined,
                            color: selectedStatuses.contains(status)
                                ? const Color(0xFF00A19A)
                                : Colors.grey.shade400,
                          )
                        ],
                      ),
                    ),
                  ),
                )
                .toList(),
            const Divider(color: Color.fromRGBO(13, 13, 13, 0.10)),
            const SizedBox(height: 16),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 0,
                minimumSize: const Size.fromHeight(52),
                maximumSize: const Size.fromHeight(52),
              ),
              onPressed: () {
                context
                    .read<OrdersCubit>()
                    .applyFilter(sort: sort, statuses: selectedStatuses);
                Navigator.pop(context);
              },
              child: const Text("Filter"),
            )
          ],
        ),
      ),
    );
  }
}
