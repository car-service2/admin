import 'package:admin/data/datasources/remote/api/saved_orders_api_provider.dart';
import 'package:admin/data/mappers/order_mapper.dart';
import 'package:admin/data/repositories/orders_repository.dart';
import 'package:admin/domain/entities/driver.dart';
import 'package:admin/domain/usecases/orders_use_case.dart';
import 'package:admin/presentation/pages/set_order_page/set_order_page.dart';
import 'package:admin/presentation/widgets/confirm_dialog.dart';
import 'package:admin/presentation/widgets/failure_widget.dart';
import 'package:admin/presentation/widgets/loader_widget.dart';
import 'package:admin/presentation/widgets/select_driver_bottom_sheet.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../../../domain/entities/order.dart';
import '../../../../service_locator.dart';
import '../../../blocs/create_order/create_order_cubit.dart';
import '../../../blocs/orders/orders_cubit.dart';
import 'order_list_tile.dart';

class OrderList extends StatefulWidget {
  const OrderList({super.key});

  @override
  State<OrderList> createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  final ScrollController _scrollController = ScrollController();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    BlocProvider.of<OrdersCubit>(context).refresh();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScroll);
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.atEdge &&
        _scrollController.position.pixels != 0) {
      // Достигнут конец списка
      _loadData();
    }
  }

  Future<void> _loadData() async {
    final ordersCubit = BlocProvider.of<OrdersCubit>(context);

    if (!_isLoading) {
      // Предотвращаем повторную загрузку данных во время подгрузки
      setState(() => _isLoading = true);

      try {
        await ordersCubit.getOrders(); // Загружаем данные из кубита
      } catch (e) {
        debugPrint(e.toString());
      }

      // Завершаем подгрузку
      setState(() => _isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrdersCubit, OrdersState>(
      builder: (context, state) {
        if (state is OrdersLoaded) {
          return SafeArea(
            child: RefreshIndicator.adaptive(
              onRefresh: _onRefresh,
              child: NotificationListener<ScrollNotification>(
                onNotification: (notification) {
                  // Добавляем обработчик прокрутки для определения конца списка
                  if (!_isLoading &&
                      notification.metrics.pixels >=
                          notification.metrics.maxScrollExtent) {
                    _loadData();
                  }
                  return false;
                },
                child: SlidableAutoCloseBehavior(
                  child: ListView.builder(
                    physics: const AlwaysScrollableScrollPhysics(),
                    controller: _scrollController,
                    itemCount: _isLoading
                        ? state.orders.length + 1
                        : state.orders.length,
                    itemBuilder: (context, index) {
                      if (index < state.orders.length) {
                        // Отображаем элемент списка
                        final order = state.orders[index];

                        final VoidCallback? onSavedOrder = order.status ==
                                OrderStatusEntity.saved
                            ? () {
                                SelectDriverBottomSheet.show<DriverEntity>(
                                        context)
                                    .then((DriverEntity? driver) {
                                  if (driver != null) {
                                    var createOrderCubit =
                                        locator.get<CreateOrderCubit>();

                                    createOrderCubit
                                        .update(order.copyWith(driver: driver))
                                        .then((value) =>
                                            BlocProvider.of<OrdersCubit>(
                                                    context)
                                                .refresh());
                                  }
                                });
                              }
                            : null;

                        void onEdit() async {
                          await SetOrderPage.updateOrder(
                            context,
                            order: order,
                          );
                        }

                        void onDelete() {
                          ConfirmDialog.show(
                            context: context,
                            title: "Confirm delete",
                            content: "Do you want delete order id:${order.id}",
                            onYes: () {
                              BlocProvider.of<OrdersCubit>(context)
                                  .delete(order.id);
                            },
                          );
                        }

                        void onAddToSaved() {
                          final createSavedOrderCubit = CreateOrderCubit(
                            OrdersUseCase(
                              OrdersRepositoryImpl(
                                SavedOrdersApiProvider(
                                    locator.get<Dio>(), apiBaseUrl),
                                locator.get<OrderMapper>(),
                              ),
                            ),
                          );
                          createSavedOrderCubit.create(order);
                          Future.delayed(
                            const Duration(milliseconds: 500),
                            () {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(
                                      "Order id:${order.id} added to saved list"),
                                  dismissDirection: DismissDirection.up,
                                  behavior: SnackBarBehavior.floating,
                                  margin: const EdgeInsets.only(
                                    bottom: 10,
                                    left: 10,
                                    right: 10,
                                  ),
                                ),
                              );
                            },
                          );
                        }

                        if (order.status == OrderStatusEntity.saved) {
                          return BlocProvider(
                            create: (context) =>
                                locator.get<CreateOrderCubit>(),
                            child: OrderListTile(
                              order: order,
                              onEdit: onEdit,
                              onDelete: onDelete,
                              onSetDriver: onSavedOrder,
                              onAddToSaved: onAddToSaved,
                            ),
                          );
                        }

                        return OrderListTile(
                          order: order,
                          onEdit: onEdit,
                          onDelete: onDelete,
                          onSetDriver: onSavedOrder,
                          onAddToSaved: onAddToSaved,
                        );
                      } else {
                        // Достигли конца списка, показываем индикатор загрузки
                        return _isLoading
                            ? const Center(
                                child: SizedBox.square(
                                  dimension: 25,
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                    strokeWidth: 1.5,
                                  ),
                                ),
                              )
                            : const SizedBox();
                      }
                    },
                  ),
                ),
              ),
            ),
          );
        }

        if (state is OrdersFailure) {
          return Center(
            child: FailureWidget(
              message: state.message,
              onPressed: () {},
            ),
          );
        }

        return const Center(child: LoaderWidget());
      },
    );
  }

  Future<void> _onRefresh() async {
    // monitor network fetch
    await BlocProvider.of<OrdersCubit>(context).refresh(
      statuses: [OrderStatusEntity.all],
      orderSort: OrderSortEntity.desc,
    );
  }
}
