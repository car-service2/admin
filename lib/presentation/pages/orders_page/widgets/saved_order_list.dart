import 'package:admin/presentation/pages/set_order_page/set_order_page.dart';
import 'package:admin/presentation/widgets/confirm_dialog.dart';
import 'package:admin/presentation/widgets/failure_widget.dart';
import 'package:admin/presentation/widgets/loader_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../../../domain/entities/order.dart';
import '../../../blocs/orders/orders_cubit.dart';
import 'order_list_tile.dart';

class SavedOrderList extends StatefulWidget {
  const SavedOrderList({super.key});

  @override
  State<SavedOrderList> createState() => _OrderListState();
}

class _OrderListState extends State<SavedOrderList> {
  final ScrollController _scrollController = ScrollController();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    BlocProvider.of<OrdersCubit>(context).refresh();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScroll);
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.atEdge &&
        _scrollController.position.pixels != 0) {
      // Достигнут конец списка
      _loadData();
    }
  }

  Future<void> _loadData() async {
    final ordersCubit = BlocProvider.of<OrdersCubit>(context);

    if (!_isLoading) {
      // Предотвращаем повторную загрузку данных во время подгрузки
      setState(() => _isLoading = true);

      try {
        await ordersCubit.getOrders(); // Загружаем данные из кубита
      } catch (e) {
        debugPrint(e.toString());
      }

      // Завершаем подгрузку
      setState(() => _isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrdersCubit, OrdersState>(
      builder: (context, state) {
        if (state is OrdersLoaded) {
          return SafeArea(
            child: RefreshIndicator.adaptive(
              onRefresh: _onRefresh,
              child: NotificationListener<ScrollNotification>(
                onNotification: (notification) {
                  // Добавляем обработчик прокрутки для определения конца списка
                  if (!_isLoading &&
                      notification.metrics.pixels >=
                          notification.metrics.maxScrollExtent) {
                    _loadData();
                  }
                  return false;
                },
                child: SlidableAutoCloseBehavior(
                  child: ListView.builder(
                    physics: const AlwaysScrollableScrollPhysics(),
                    controller: _scrollController,
                    itemCount: _isLoading
                        ? state.orders.length + 1
                        : state.orders.length,
                    itemBuilder: (context, index) {
                      if (index < state.orders.length) {
                        // Отображаем элемент списка
                        final order = state.orders[index];

                        void onEdit() {
                          SetOrderPage.updateSavedOrder<bool>(
                            context,
                            savedOrder: order,
                          ).then((value) {
                            BlocProvider.of<OrdersCubit>(context).refresh();
                          });
                        }

                        void onDelete() {
                          ConfirmDialog.show(
                            context: context,
                            title: "Confirm delete",
                            content:
                                "Do you want delete saved order id:${order.id}",
                            onYes: () {
                              BlocProvider.of<OrdersCubit>(context)
                                  .delete(order.id);
                            },
                          );
                        }

                        void onCreateOrder() async {
                          await SetOrderPage.createFromSavedOrder(
                            context,
                            savedOrder: order,
                          );
                        }

                        return OrderListTile(
                          order: order,
                          onEdit: onEdit,
                          onDelete: onDelete,
                          onCreateOrder: onCreateOrder,
                        );
                      } else {
                        // Достигли конца списка, показываем индикатор загрузки
                        return _isLoading
                            ? const Center(
                                child: SizedBox.square(
                                  dimension: 25,
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                    strokeWidth: 1.5,
                                  ),
                                ),
                              )
                            : Container();
                      }
                    },
                  ),
                ),
              ),
            ),
          );
        }

        if (state is OrdersFailure) {
          return Center(
            child: FailureWidget(
              message: state.message,
              onPressed: () {},
            ),
          );
        }

        return const Center(child: LoaderWidget());
      },
    );
  }

  Future<void> _onRefresh() async {
    await BlocProvider.of<OrdersCubit>(context).refresh(
      statuses: [OrderStatusEntity.all],
    );
  }
}
