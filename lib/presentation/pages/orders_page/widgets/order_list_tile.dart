import 'package:admin/domain/entities/order.dart';
import 'package:admin/presentation/blocs/create_order/create_order_cubit.dart';
import 'package:admin/presentation/pages/orders_page/widgets/order_dialog.dart';
import 'package:admin/presentation/theme/app_theme.dart';
import 'package:admin/presentation/widgets/loader_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class OrderListTile extends StatelessWidget {
  final OrderEntity order;
  final VoidCallback onEdit;
  final VoidCallback onDelete;
  final VoidCallback? onSetDriver;
  final VoidCallback? onAddToSaved;
  final VoidCallback? onCreateOrder;

  const OrderListTile({
    super.key,
    required this.order,
    required this.onEdit,
    required this.onDelete,
    this.onSetDriver,
    this.onAddToSaved,
    this.onCreateOrder,
  });

  @override
  Widget build(BuildContext context) {
    var orderListTile = Slidable(
      key: ValueKey(order.id),
      closeOnScroll: true,
      endActionPane: ActionPane(
        extentRatio: 1.0,
        motion: const ScrollMotion(),
        children: [
          SlidableAction(
            backgroundColor: Colors.transparent,
            onPressed: (context) => onEdit(),
            icon: Icons.edit,
            label: 'Edit',
          ),
          SlidableAction(
            backgroundColor: Colors.transparent,
            onPressed: (context) => onDelete(),
            icon: Icons.delete_forever,
            label: 'Delete',
          ),
          if (onSetDriver != null)
            SlidableAction(
              backgroundColor: Colors.transparent,
              onPressed: (context) => onSetDriver!(),
              icon: Icons.drive_eta,
              label: 'Set Driver',
            ),
          if (onAddToSaved != null)
            SlidableAction(
              backgroundColor: Colors.transparent,
              onPressed: (context) => onAddToSaved!(),
              icon: Icons.list,
              label: 'Add to saved',
            ),
          if (onCreateOrder != null)
            SlidableAction(
              backgroundColor: Colors.transparent,
              onPressed: (context) => onCreateOrder!(),
              icon: Icons.add_circle,
              label: 'Create order',
            ),
        ],
      ),
      child: GestureDetector(
        onTap: () {
          OrderDialog.show(context, order);
        },
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 7),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(22),
          ),
          padding: const EdgeInsets.symmetric(
            vertical: 14,
            horizontal: 15,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /// id , progress, menu:edit,delete
              Row(
                children: [
                  Text(
                    "Order ID: ",
                    style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: const Color.fromRGBO(13, 13, 13, 0.50),
                    ),
                  ),
                  Text(
                    "${order.id}",
                    style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: const Color(0xFF0D0D0D),
                    ),
                  ),
                  const Spacer(),
                  if (order.toll)
                    const Icon(
                      Icons.toll,
                      color: Colors.deepOrange,
                    ),
                  if (order.important)
                    const Icon(
                      Icons.priority_high,
                      color: Colors.red,
                    ),
                  if (order.female)
                    const Icon(
                      Icons.female,
                      color: Colors.pinkAccent,
                    ),
                  const SizedBox(width: 5),
                  if (order.status != null)
                    Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 5,
                        horizontal: 10,
                      ),
                      decoration: BoxDecoration(
                        color: order.status!.color,
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Text(
                        order.status.toString(),
                        style: GoogleFonts.montserrat(
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                          color: order.status!.foreground,
                        ),
                      ),
                    ),
                ],
              ),

              /// locations
              Column(
                children: order.waypoints.map((waypoint) {
                  bool isStart = order.waypoints.first == waypoint;
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4),
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          isStart
                              ? "assets/icons/focus-3-line.svg"
                              : "assets/icons/map-pin-2-line.svg",
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Text(
                            waypoint.address,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                              color: Colors.black,
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                }).toList(),
              ),
              const SizedBox(height: 16),

              if (order.description != null &&
                  order.description?.isNotEmpty == true)
                Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  margin: const EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: Colors.grey.shade200,
                  ),
                  child: Text(order.description!),
                ),

              /// tags,
              Wrap(
                crossAxisAlignment: WrapCrossAlignment.start,
                runAlignment: WrapAlignment.start,
                spacing: 5,
                runSpacing: 5,
                children: [
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                    decoration: BoxDecoration(
                      color: const Color(0xFF57BF9C),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Text(
                      order.createdAt,
                      style: GoogleFonts.montserrat(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                      ),
                    ),
                  ),
                  if (order.waypoints.isNotEmpty)
                    Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 6, horizontal: 10),
                      decoration: BoxDecoration(
                        color: const Color(0xFF57BF9C),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Text(
                        "Passengers: ${order.waypoints.first.passengers}",
                        style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  if (order.driver != null)
                    Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 6, horizontal: 10),
                      decoration: BoxDecoration(
                        color: const Color(0xFF57BF9C),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Text(
                        "${order.driver?.carInfo}",
                        style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  if (order.driver != null)
                    Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 6, horizontal: 10),
                      decoration: BoxDecoration(
                        color: const Color(0xFF57BF9C),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Text(
                        "${order.driver?.gender}",
                        style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                        ),
                      ),
                    )
                ],
              ),
            ],
          ),
        ),
      ),
    );

    if (order.status == OrderStatusEntity.saved) {
      return BlocBuilder<CreateOrderCubit, CreateOrderState>(
        builder: (context, state) {
          bool loading = state is CreateOrderLoading;
          return Stack(
            children: [
              IgnorePointer(
                ignoring: loading,
                child: orderListTile,
              ),
              if (loading)
                Positioned(
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                  child: Center(
                    child: LoaderWidget(color: AppColors.background),
                  ),
                )
            ],
          );
        },
      );
    }

    return orderListTile;
  }
}
