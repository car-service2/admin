import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../domain/entities/driver.dart';
import '../../../../domain/entities/order.dart';
import '../../../blocs/create_order/create_order_cubit.dart';
import '../../../blocs/orders/orders_cubit.dart';
import '../../../widgets/select_driver_bottom_sheet.dart';

class OrderDialog extends StatelessWidget {
  final OrderEntity order;

  const OrderDialog({super.key, required this.order});

  static Future<T?> show<T>(BuildContext context, OrderEntity order) async {
    return showDialog<T>(
      context: context,
      builder: (_) {
        if (order.status == OrderStatusEntity.saved) {
          return BlocProvider.value(
            value: BlocProvider.of<CreateOrderCubit>(context),
            child: OrderDialog(order: order),
          );
        }
        return OrderDialog(order: order);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(16),
      backgroundColor: const Color(0xFFF6F6F6),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(34),
      ),
      child: Padding(
        padding: const EdgeInsets.all(14.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 20,
                  horizontal: 15,
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(22),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(22),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Order ID: ",
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: const Color.fromRGBO(13, 13, 13, 0.50),
                            ),
                          ),
                          Text(
                            "${order.id}",
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: const Color(0xFF0D0D0D),
                            ),
                          ),
                          const Spacer(),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.zero,
                              elevation: 0,
                              minimumSize: const Size.square(40),
                              maximumSize: const Size.square(40),
                            ),
                            onPressed: () async {
                              final telUrl =
                                  Uri.parse("tel:${order.customerPhone}");
                              if (await canLaunchUrl(telUrl)) {
                                await launchUrl(telUrl);
                              }
                            },
                            child: SvgPicture.asset(
                              "assets/icons/phone-line.svg",
                              height: 24,
                              width: 24,
                              colorFilter: const ColorFilter.mode(
                                Colors.white,
                                BlendMode.srcIn,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 6,
                            horizontal: 8,
                          ),
                          decoration: BoxDecoration(
                            color: const Color(0xFFF6F6F6),
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: Text(
                            order.createdAt,
                            style: GoogleFonts.montserrat(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        const Spacer(),
                        if (order.status != null)
                          Container(
                            padding: const EdgeInsets.symmetric(
                              vertical: 5,
                              horizontal: 10,
                            ),
                            decoration: BoxDecoration(
                              color: order.status?.color,
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Text(
                              order.status.toString(),
                              style: GoogleFonts.montserrat(
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                color: order.status?.foreground,
                              ),
                            ),
                          ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    Row(
                      children: [
                        SvgPicture.asset("assets/icons/wallet-3-line.svg"),
                        const SizedBox(width: 8),
                        Text(
                          "${order.paymentType} | ${order.paymentType == 'cash' ? order.orderValue: order.orderCreditValue} USD",
                          style: GoogleFonts.montserrat(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        const Spacer(),
                        if (order.toll)
                          const Icon(
                            Icons.toll,
                            color: Colors.deepOrange,
                          ),
                        if (order.important)
                          const Icon(
                            Icons.priority_high,
                            color: Colors.red,
                          ),
                        if (order.female)
                          const Icon(
                            Icons.female,
                            color: Colors.pinkAccent,
                          ),
                      ],
                    ),
                    if (order.description != null &&
                        order.description?.isNotEmpty == true)
                      Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 10,
                        ),
                        margin: const EdgeInsets.only(
                          bottom: 0,
                          top: 10,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Colors.grey.shade200,
                        ),
                        child: Text(order.description!),
                      ),
                    buildDriverSection(
                      driver: order.driver,
                      title: 'driver',
                    ),
                    buildDriverSection(
                      driver: order.canceledDriver,
                      title: 'canceled driver',
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildDriverSection({
    String title = 'driver',
    DriverEntity? driver,
  }) {
    if (driver == null) {
      return const SizedBox();
    }
    return Column(
      children: [
        const SizedBox(height: 14),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 14,
              ),
              decoration: BoxDecoration(
                color: const Color(0xFFF6F6F6),
                borderRadius: BorderRadius.circular(12),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: GoogleFonts.montserrat(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: const Color.fromRGBO(13, 13, 13, 0.40),
                    ),
                  ),
                  const SizedBox(height: 5),
                  Text(
                    driver.name,
                    style: GoogleFonts.montserrat(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  const SizedBox(height: 20),

                  /// tags,
                  Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 6, horizontal: 10),
                        decoration: BoxDecoration(
                          color: const Color(0xFF57BF9C),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          "ID: ${driver.id}",
                          style: GoogleFonts.montserrat(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                          ),
                        ),
                      ),
                      const SizedBox(width: 14),
                      if (driver.carInfo.isNotEmpty)
                        Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 6, horizontal: 10),
                          decoration: BoxDecoration(
                            color: const Color(0xFF57BF9C),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Text(
                            driver.carInfo,
                            style: GoogleFonts.montserrat(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      const SizedBox(width: 14),
                    ],
                  ),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Divider(),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: order.waypoints.map(
                (wp) {
                  var title = wp == order.waypoints.first
                      ? "start"
                      : (wp == order.waypoints.last ? 'finish' : 'middle');
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(6),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: const Color.fromRGBO(87, 191, 156, 0.30),
                            ),
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: const Color(0xFF57BF9C)),
                              child: SvgPicture.asset(
                                title == 'finish'
                                    ? "assets/icons/map-pin-2-line.svg"
                                    : "assets/icons/focus-3-line.svg",
                                height: 18,
                                width: 18,
                                colorFilter: const ColorFilter.mode(
                                  Colors.white,
                                  BlendMode.srcIn,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 12),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  title,
                                  style: GoogleFonts.montserrat(
                                    color: const Color.fromRGBO(
                                      13,
                                      13,
                                      13,
                                      0.60,
                                    ),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  wp.address,
                                  style: GoogleFonts.montserrat(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Round trip: ${wp.rt} / Passengers: ${wp.passengers}",
                                      style: GoogleFonts.montserrat(
                                        color: const Color.fromRGBO(
                                          13,
                                          13,
                                          13,
                                          0.60,
                                        ),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      if (wp != order.waypoints.last)
                        Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: 5,
                                width: 2,
                                margin: const EdgeInsets.all(2),
                                decoration: BoxDecoration(
                                  color: const Color.fromRGBO(13, 13, 13, 0.30),
                                  borderRadius: BorderRadius.circular(50),
                                ),
                              ),
                              Container(
                                height: 5,
                                width: 2,
                                margin: const EdgeInsets.all(2),
                                decoration: BoxDecoration(
                                  color: const Color.fromRGBO(13, 13, 13, 0.30),
                                  borderRadius: BorderRadius.circular(50),
                                ),
                              ),
                              Container(
                                height: 5,
                                width: 2,
                                margin: const EdgeInsets.all(2),
                                decoration: BoxDecoration(
                                  color: const Color.fromRGBO(13, 13, 13, 0.30),
                                  borderRadius: BorderRadius.circular(50),
                                ),
                              ),
                            ],
                          ),
                        ),
                    ],
                  );
                },
              ).toList(),
            ),
            const SizedBox(height: 5),
          ],
        ),
        const SizedBox(height: 14),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            padding: EdgeInsets.zero,
            elevation: 0,
            minimumSize: const Size.fromHeight(52),
            maximumSize: const Size.fromHeight(52),
          ),
          onPressed: () async {
            final telUrl = Uri.parse("tel:${driver.phone}");
            if (await canLaunchUrl(telUrl)) {
              await launchUrl(telUrl);
            }
          },
          child: SvgPicture.asset(
            "assets/icons/phone-line.svg",
            height: 24,
            width: 24,
            colorFilter: const ColorFilter.mode(
              Colors.white,
              BlendMode.srcIn,
            ),
          ),
        ),
      ],
    );
  }

  void onSetDriver(BuildContext context) {
    SelectDriverBottomSheet.show<DriverEntity>(context)
        .then((DriverEntity? driver) {
      if (driver != null) {
        var createOrderCubit = BlocProvider.of<CreateOrderCubit>(context);

        createOrderCubit.update(order.copyWith(driver: driver)).then((value) {
          BlocProvider.of<OrdersCubit>(context).refresh();
          Navigator.pop(context);
        });
      }
    });
  }
}
