import 'dart:async';

import 'package:admin/data/datasources/remote/api/saved_orders_api_provider.dart';
import 'package:admin/data/mappers/order_mapper.dart';
import 'package:admin/data/repositories/orders_repository.dart';
import 'package:admin/domain/usecases/orders_use_case.dart';
import 'package:admin/presentation/blocs/orders/orders_cubit.dart';
import 'package:admin/presentation/pages/orders_page/widgets/saved_order_list.dart';
import 'package:admin/service_locator.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SavedOrdersPage extends StatefulWidget {
  const SavedOrdersPage({super.key});

  @override
  State<SavedOrdersPage> createState() => _SavedOrdersPageState();
}

class _SavedOrdersPageState extends State<SavedOrdersPage> {
  final _searchController = TextEditingController();
  Timer? _debounceTimer;

  @override
  void initState() {
    _searchController.text = BlocProvider.of<OrdersCubit>(context).search;
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    _debounceTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Saved Orders"),
      ),
      body: BlocProvider<OrdersCubit>(
        create: (_) {
          return OrdersCubit(
            OrdersUseCase(
              OrdersRepositoryImpl(
                SavedOrdersApiProvider(locator.get<Dio>(), apiBaseUrl),
                locator.get<OrderMapper>(),
              ),
            ),
          );
        },
        child: const Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SavedOrderList(),
        ),
      ),
    );
  }
}
