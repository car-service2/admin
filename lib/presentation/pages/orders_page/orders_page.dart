import 'dart:async';

import 'package:admin/domain/entities/order.dart';
import 'package:admin/presentation/blocs/orders/orders_cubit.dart';
import 'package:admin/presentation/pages/orders_page/widgets/order_filter_sheet.dart';
import 'package:admin/presentation/pages/orders_page/widgets/order_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../widgets/search_input.dart';

class OrdersPage extends StatefulWidget {
  const OrdersPage({super.key});

  @override
  State<OrdersPage> createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  final _searchController = TextEditingController();
  Timer? _debounceTimer;

  @override
  void initState() {
    _searchController.text = BlocProvider.of<OrdersCubit>(context).search;
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    _debounceTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          SizedBox(
            height: 52,
            child: Row(
              children: [
                Expanded(
                  child: SearchInput(
                    controller: _searchController,
                    hint: "Search",
                    onSearchTextChanged: _onSearchTextChanged,
                  ),
                ),
                const SizedBox(width: 10),
                BlocBuilder<OrdersCubit, OrdersState>(
                  builder: (context, state) {
                    if (state is OrdersLoaded) {
                      return GestureDetector(
                        onTap: () async {
                          await OrdersFilterSheet.show<List<OrderStatusEntity>>(
                            context,
                            filters: state.orderStatus,
                            orderSort: state.orderSort,
                          );
                        },
                        child: Container(
                          padding: const EdgeInsets.all(14),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: state.allFiltered
                                ? Colors.white
                                : Colors.yellow,
                          ),
                          child: SvgPicture.asset("assets/icons/filter.svg"),
                        ),
                      );
                    }

                    return Container(
                      padding: const EdgeInsets.all(14),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.white,
                      ),
                      child: SvgPicture.asset("assets/icons/filter.svg"),
                    );
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          const Expanded(child: OrderList()),
        ],
      ),
    );
  }

  void _onSearchTextChanged(String text) {
    _debounceTimer?.cancel();

    // Создаем новый таймер, чтобы вызвать обработчик через 500 миллисекунд
    _debounceTimer = Timer(const Duration(milliseconds: 500), () {
      // Здесь можно выполнить ваш поиск или другие действия с текстом
      debugPrint("Выполняем поиск $text");
      BlocProvider.of<OrdersCubit>(context).applySearch(text);
    });
  }
}
