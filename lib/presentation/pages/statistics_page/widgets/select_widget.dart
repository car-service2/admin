import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class SelectWidget extends StatelessWidget {
  final String title;
  final String icon;
  final String? selected;
  final VoidCallback onSelect;

  const SelectWidget({
    super.key,
    required this.title,
    required this.icon,
    required this.onSelect,
    required this.selected,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onSelect,
      child: Container(
        decoration: BoxDecoration(
          color: const Color(0xFFF6F6F6),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 14),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    title,
                    style: GoogleFonts.montserrat(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SvgPicture.asset("assets/icons/arrow-right-s-line.svg"),
                ],
              ),
            ),
            const Divider(height: 0),
            if (selected != null)
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 16,
                  horizontal: 14,
                ),
                decoration: BoxDecoration(
                  color: const Color(0xFFF6F6F6),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      icon,
                      height: 20,
                      width: 20,
                    ),
                    const SizedBox(width: 14),
                    Expanded(
                      child: Text(
                        selected.toString(),
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    const Icon(
                      Icons.radio_button_checked,
                      color: Color(0xFF57BF9C),
                    ),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
