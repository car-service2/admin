import 'package:admin/service_locator.dart';
import 'package:admin/utils/num_extention.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../domain/entities/driver.dart';
import '../../../domain/entities/organization.dart';
import '../../blocs/statistics/statistics_cubit.dart';
import '../../widgets/date_range_input.dart';
import '../../widgets/select_driver_bottom_sheet.dart';
import '../../widgets/statistic_tile.dart';
import '../set_order_page/organizations_page.dart';
import 'widgets/select_widget.dart';

class StatisticsPage extends StatefulWidget {
  const StatisticsPage({super.key});

  @override
  State<StatisticsPage> createState() => _StatisticsPageState();
}

class _StatisticsPageState extends State<StatisticsPage> {
  late StatisticsCubit _driverStatisticCubit;
  late StatisticsCubit _companyStatisticCubit;
  DriverEntity? driver;
  OrganizationEntity? organization;
  late DateTime _driverStatisticStartDate;
  late DateTime _driverStatisticEndDate;
  late DateTime _companyStatisticStartDate;
  late DateTime _companyStatisticEndDate;

  @override
  void initState() {
    _driverStatisticCubit = locator.get<StatisticsCubit>();
    _companyStatisticCubit = locator.get<StatisticsCubit>();

    final startOfDay = DateUtils.dateOnly(DateTime.now());
    final endOfDay = DateUtils.dateOnly(DateTime.now());
    _driverStatisticStartDate = startOfDay;
    _driverStatisticEndDate = endOfDay;
    _companyStatisticStartDate = startOfDay;
    _companyStatisticEndDate = endOfDay;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _driverStatisticCubit.close();
    _companyStatisticCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(20),
      child: SafeArea(
        child: Column(
          children: [
            BlocConsumer<StatisticsCubit, StatisticsState>(
              bloc: _driverStatisticCubit,
              builder: (context, state) {
                final statistic =
                    state is StatisticsLoaded ? state.statistic : null;
                return Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(24),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(22),
                  ),
                  child: Column(
                    children: [
                      SelectWidget(
                        title: 'Choose driver',
                        icon: 'assets/icons/user-line.svg',
                        onSelect: () async {
                          final driver =
                              await SelectDriverBottomSheet.show<DriverEntity>(
                                  context);
                          if (driver != null) {
                            setState(() => this.driver = driver);
                            getDriverStatistics();
                          }
                        },
                        selected: driver?.name,
                      ),
                      const SizedBox(height: 20),
                      DateRangeInput(
                        initialStart: _driverStatisticStartDate,
                        initialEnd: _driverStatisticEndDate,
                        onChanged: (DateTime start, DateTime end) {
                          _driverStatisticStartDate = start;
                          _driverStatisticEndDate = end;
                          getDriverStatistics();
                        },
                      ),
                      const SizedBox(height: 10),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          color: const Color(0xFFF6F6F6),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Column(
                          children: [
                            const SizedBox(height: 5),
                            Text(
                              "${statistic?.total.formatWithTwoDecimals() ?? '--'} USD",
                              style: GoogleFonts.montserrat(
                                fontWeight: FontWeight.w700,
                                fontSize: 28,
                              ),
                            ),
                            Text(
                              "${statistic?.count ?? '--'} orders",
                              style: GoogleFonts.montserrat(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: const Color(0xFF0D0D0D).withOpacity(0.4),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 20),
                      StatisticTileWidget(
                        title: "Cash",
                        value:
                            "${statistic?.cash.formatWithTwoDecimals() ?? '--'} USD",
                      ),
                      Divider(
                        color: const Color(0xFF0D0D0D).withOpacity(0.09),
                        thickness: 1,
                      ),
                      StatisticTileWidget(
                        title: "Credit card",
                        value:
                            "${statistic?.credit.formatWithTwoDecimals() ?? '--'} USD",
                      ),
                      Divider(
                        color: const Color(0xFF0D0D0D).withOpacity(0.09),
                        thickness: 1,
                      ),
                      StatisticTileWidget(
                        title: "Toll",
                        value:
                            "${statistic?.toll.formatWithTwoDecimals() ?? '--'} USD",
                      ),
                      Divider(
                        color: const Color(0xFF0D0D0D).withOpacity(0.09),
                        thickness: 1,
                      ),
                      StatisticTileWidget(
                        title: "Company percentage",
                        value:
                            "${statistic?.percentage.formatWithTwoDecimals() ?? '--'} USD",
                      ),
                    ],
                  ),
                );
              },
              listener: (context, state) {
                if (state is StatisticsFailure) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Error retrieving driver statistics."),
                    ),
                  );
                }
              },
            ),
            const SizedBox(height: 20),
            BlocConsumer<StatisticsCubit, StatisticsState>(
              bloc: _companyStatisticCubit,
              builder: (context, state) {
                final statistic =
                    state is StatisticsLoaded ? state.statistic : null;
                return Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(24),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(22),
                  ),
                  child: Column(
                    children: [
                      SelectWidget(
                        title: 'Choose company',
                        icon: 'assets/icons/passport-line.svg',
                        onSelect: () async {
                          final organization = await OrganizationsBottomSheet
                              .show<OrganizationEntity>(context);
                          if (organization != null) {
                            setState(() => this.organization = organization);
                            getCompanyStatistics();
                          }
                        },
                        selected: organization?.name,
                      ),
                      const SizedBox(height: 20),
                      DateRangeInput(
                        initialStart: _companyStatisticStartDate,
                        initialEnd: _companyStatisticEndDate,
                        onChanged: (DateTime start, DateTime end) {
                          _companyStatisticStartDate = start;
                          _companyStatisticEndDate = end;
                          getCompanyStatistics();
                        },
                      ),
                      const SizedBox(height: 10),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          color: const Color(0xFFF6F6F6),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Column(
                          children: [
                            const SizedBox(height: 5),
                            Text(
                              "${statistic?.total.formatWithTwoDecimals() ?? '--'} USD",
                              style: GoogleFonts.montserrat(
                                fontWeight: FontWeight.w700,
                                fontSize: 28,
                              ),
                            ),
                            Text(
                              "${statistic?.count ?? '--'} orders",
                              style: GoogleFonts.montserrat(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: const Color(0xFF0D0D0D).withOpacity(0.4),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
              listener: (context, state) {
                if (state is StatisticsFailure) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Error retrieving company statistics."),
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> getDriverStatistics() async {
    if (driver != null) {
      await _driverStatisticCubit.getDriverStatistics(
        driver!,
        _driverStatisticStartDate,
        _driverStatisticEndDate,
      );
    }
  }

  Future<void> getCompanyStatistics() async {
    if (organization != null) {
      await _companyStatisticCubit.getOrganizationStatistics(
        organization!,
        _companyStatisticStartDate,
        _companyStatisticEndDate,
      );
    }
  }
}
