import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

import '../../../domain/entities/user.dart';
import '../../../domain/usecases/auth_use_case.dart';

part 'auth_event.dart';

part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthUseCase _authUseCase;

  AuthBloc(this._authUseCase) : super(AuthInitial()) {
    on<AuthCheckAuthenticationEvent>(_onAppStarted);
  }

  FutureOr<void> _onAppStarted(
    AuthCheckAuthenticationEvent event,
    Emitter<AuthState> emit,
  ) async {
    emit(AuthLoading());

    try {
      final user = await _authUseCase.getUser();
      emit(AuthAuthenticated(user));
    } on DioException catch (e) {
      return switch (e.type) {
        DioExceptionType.badResponse => emit(AuthUnauthenticated()),
        _ => emit(AuthError(e.message ?? e.toString())),
      };
    } catch (e) {
      emit(AuthError(e.toString()));
    }
  }
}
