part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  String get initialRoute;
}

class AuthInitial extends AuthState {
  @override
  List<Object?> get props => [];

  @override
  String get initialRoute => 'loader';
}

class AuthLoading extends AuthState {
  @override
  List<Object?> get props => [];

  @override
  String get initialRoute => 'loader';
}

class AuthAuthenticated extends AuthState {
  final UserEntity user;

  const AuthAuthenticated(this.user);

  @override
  List<Object?> get props => [user];

  @override
  String get initialRoute => 'main';
}

class AuthUnauthenticated extends AuthState {
  @override
  List<Object?> get props => [];

  @override
  String get initialRoute => 'signIn';
}

class AuthError extends AuthState {
  final String message;

  const AuthError(this.message);

  @override
  List<Object?> get props => [message];

  @override
  String get initialRoute => 'loader';
}
