import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class DriverFilter extends Equatable {
  final String? search;
  final bool? isOnline;
  final bool? isMale;
  final int? addressId;

  @override
  List<Object?> get props => [search, isOnline, isMale, addressId];

  const DriverFilter({
    required this.search,
    required this.isOnline,
    required this.isMale,
    required this.addressId,
  });

  factory DriverFilter.initial() {
    return const DriverFilter(
      search: null,
      isOnline: null,
      isMale: null,
      addressId: null,
    );
  }

  bool get isApplied =>
      toQueryParam().keys.where((element) => element != 'search').isNotEmpty;

  DriverFilter copyWith({
    String? search,
    bool? isOnline,
    bool? isMale,
    int? addressId,
  }) {
    return DriverFilter(
      search: search ?? this.search,
      isOnline: isOnline ?? this.isOnline,
      isMale: isMale ?? this.isMale,
      addressId: addressId ?? this.addressId,
    );
  }

  Map<String, dynamic> toQueryParam() {
    final params = <String, dynamic>{};

    if (search != null && search!.isNotEmpty) {
      params['search'] = search;
    } else {
      if (isOnline != null) {
        params['in_line'] = isOnline! ? 'true' : 'false';
      }

      if (isMale != null) {
        params['gender'] = isMale! ? 'male' : 'female';
      }

      if (addressId != null) {
        params['stop_address_id'] = addressId;
      }
    }

    debugPrint("params: $params");
    return params;
  }
}
