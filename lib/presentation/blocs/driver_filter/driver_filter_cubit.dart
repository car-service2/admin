import 'package:admin/presentation/blocs/addresses/addresses_cubit.dart';
import 'package:admin/presentation/blocs/drivers/drivers_cubit.dart';
import 'package:bloc/bloc.dart';

import 'driver_filter.dart';

class DriverFilterCubit extends Cubit<DriverFilter> {
  final DriversCubit _driversCubit;
  final AddressesCubit _addressesCubit;

  DriverFilterCubit(
    this._driversCubit,
    this._addressesCubit,
  ) : super(DriverFilter.initial());

  void setSearch(String? query) {
    emit(DriverFilter(
      search: query,
      isOnline: state.isOnline,
      isMale: state.isMale,
      addressId: state.addressId,
    ));
    _applyFilters();
  }

  void setAddress(int? addressId) {
    emit(DriverFilter(
      search: state.search,
      isOnline: state.isOnline,
      isMale: state.isMale,
      addressId: addressId,
    ));
    _applyFilters();
  }

  void setFilter(DriverFilter filter) {
    emit(filter);
    _applyFilters();
  }

  void _applyFilters() {
    _addressesCubit.load();
    _driversCubit.setFilter(state);
  }
}
