import 'package:admin/domain/entities/driver.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/entities/order.dart';
import '../../../domain/usecases/orders_use_case.dart';

part 'orders_state.dart';

class OrdersCubit extends Cubit<OrdersState> {
  final OrdersUseCase _ordersUseCase;
  List<OrderStatusEntity> _orderStatuses = [
    OrderStatusEntity.saved,
    OrderStatusEntity.pending,
    OrderStatusEntity.canceled,
  ];
  OrderSortEntity _orderSort = OrderSortEntity.asc;

  var _page = 1;
  final _perPage = 20;
  String _search = "";

  OrdersCubit(this._ordersUseCase) : super(OrdersInitial());

  String get search => _search;

  Future<void> refresh({
    List<OrderStatusEntity>? statuses,
    OrderSortEntity? orderSort,
  }) async {
    await applyFilter(
      statuses: statuses ?? _orderStatuses,
      sort: orderSort ?? _orderSort,
    );
  }

  Future<void> getOrders() async {
    if (state is OrdersLoaded && (state as OrdersLoaded).allLoaded) {
      return;
    }
    if (state is! OrdersLoaded) {
      _page = 1;
    }

    String sortDirection = _orderSort.name;
    if (search.isNotEmpty) {
      sortDirection = OrderSortEntity.desc.name;
    }

    try {
      debugPrint("$_page | $_perPage | $_orderStatuses");
      final orders = await _ordersUseCase.getOrders(
        queryParams: {
          "sort": "date,$sortDirection",
          if (search.trim().isEmpty)
            "status": _orderStatuses.map((e) => e.toQueryParam()).join(","),
          "page": _page,
          "perPage": _perPage,
          "search": _search,
        },
      );

      bool allLoaded = _perPage > orders.length;

      OrdersLoaded ordersLoadedState = OrdersLoaded(
        orders,
        _orderStatuses,
        allLoaded,
        _orderSort,
      );
      if (state is OrdersLoaded) {
        ordersLoadedState = OrdersLoaded(
          [
            ...(state as OrdersLoaded).orders,
            ...orders,
          ],
          _orderStatuses,
          allLoaded,
          _orderSort,
        );
      }
      emit(ordersLoadedState);
      _page++;
    } catch (e) {
      _page = 1;
      debugPrint(e.toString());
      emit(OrdersFailure(e.toString()));
    }
  }

  Future<void> applyFilter({
    required List<OrderStatusEntity> statuses,
    required OrderSortEntity sort,
  }) async {
    emit(OrdersInitial());
    _orderStatuses = statuses;
    _orderSort = sort;
    await getOrders();
  }

  Future<void> applySearch(String search) async {
    emit(OrdersInitial());
    _search = search;
    await getOrders();
  }

  Future<void> delete(int id) async {
    try {
      await _ordersUseCase.delete(id);
    } catch (e) {
      debugPrint(e.toString());
      emit(OrdersFailure(e.toString()));
    }
    emit(OrdersInitial());
    await getOrders();
  }
}

class DriverCurrentOrdersCubit extends OrdersCubit {
  final DriverEntity driver;

  DriverCurrentOrdersCubit(super.ordersUseCase, this.driver);

  @override
  Future<void> getOrders() async {
    try {
      final driverCurrentOrders =
          await _ordersUseCase.getDriverCurrentOrders(driver);

      emit(OrdersLoaded(
        driverCurrentOrders,
        const [],
        true,
        _orderSort,
      ));
    } catch (e) {
      print(e);
      emit(OrdersFailure(e.toString()));
    }
  }
}
