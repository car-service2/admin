part of 'orders_cubit.dart';

@immutable
abstract class OrdersState {}

class OrdersInitial extends OrdersState {}

class OrdersFailure extends OrdersState {
  final String message;

  OrdersFailure(this.message);
}

class OrdersLoaded extends OrdersState {
  final List<OrderEntity> orders;
  final List<OrderStatusEntity> orderStatus;
  final bool allLoaded;
  final OrderSortEntity orderSort;

  OrdersLoaded(
    this.orders,
    this.orderStatus,
    this.allLoaded,
    this.orderSort,
  );

  get allFiltered =>
      orderStatus.isEmpty ||
      (orderStatus.length == 1 && orderStatus.first == OrderStatusEntity.all);
}
