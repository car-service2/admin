import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/usecases/sign_in_use_case.dart';
import '../auth/auth_bloc.dart';

part 'sign_in_state.dart';

class SignInCubit extends Cubit<SignInState> {
  final SignInUseCase _signInUseCase;
  final AuthBloc _authBloc;

  SignInCubit(this._signInUseCase, this._authBloc) : super(SignInInitial());

  Future<void> signIn({
    required String email,
    required String password,
  }) async {
    try {
      emit(SignInLoading());

      final isSignedIn = await _signInUseCase.signIn(
        email: email,
        password: password,
      );

      if (isSignedIn) {
        _authBloc.add(AuthCheckAuthenticationEvent());
      }

      emit(SignInSuccess());
    } catch (e) {
      debugPrint(e.toString());
      emit(SignInFailure());
    }
  }
}
