import 'package:admin/domain/entities/order.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/usecases/orders_use_case.dart';

part 'create_order_state.dart';

class CreateOrderCubit extends Cubit<CreateOrderState> {
  final OrdersUseCase _orderUseCase;

  CreateOrderCubit(this._orderUseCase) : super(CreateOrderInitial());

  Future<void> create(OrderEntity order) async {
    try {
      emit(CreateOrderLoading());

      final newOrder = await _orderUseCase.create(order);

      emit(CreateOrderCreated(newOrder));
    } catch (e) {
      debugPrint(e.toString());
      emit(CreateOrderFailure());
    }
  }

  Future<void> update(OrderEntity order) async {
    try {
      emit(CreateOrderLoading());

      var updatedOrder = await _orderUseCase.update(order);

      emit(CreateOrderCreated(updatedOrder));
    } catch (e) {
      debugPrint(e.toString());
      emit(CreateOrderFailure());
    }
  }
}
