part of 'drivers_cubit.dart';

@immutable
abstract class DriversState {}

class DriversInitial extends DriversState {}

class DriversLoading extends DriversState {}

class DriversLoaded extends DriversState {
  final List<DriverEntity> drivers;
  final Map<String, dynamic> filters;
  final DriverFilter filter;
  final bool allLoaded;

  DriversLoaded({
    required this.drivers,
    required this.filters,
    required this.allLoaded,
    required this.filter,
  });
}

class DriversFailure extends DriversState {
  final String message;

  DriversFailure(this.message);
}
