import 'dart:async';

import 'package:admin/data/datasources/remote/websockets/driver_websocket.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/entities/driver.dart';
import '../../../domain/usecases/drivers_use_case.dart';

part 'drivers_state.dart';

class DriversCubit extends Cubit<DriversState> {
  final DriversUseCase _driverUseCase;
  final DriverWebSocket _driverWebSocket;
  int _currentPage = 1;
  final int _perPage = 20;

  Map<String, dynamic> get filters => _filter.toQueryParam();
  DriverFilter _filter = DriverFilter.initial();

  String get search => filters['search'] ?? '';

  DriversCubit(
    this._driverUseCase,
    this._driverWebSocket,
  ) : super(DriversInitial());

  StreamSubscription<DriverEntity>? streamSubscription;

  Future<void> getDrivers() async {
    if (state is DriversLoaded && (state as DriversLoaded).allLoaded) {
      return;
    }
    if (state is! DriversLoaded) {
      _currentPage = 1;
    }

    try {
      final drivers = await _driverUseCase.getDrivers(
        queryParams: {'page': _currentPage, 'perPage': _perPage, ...filters},
      );

      if (state is DriversLoaded) {
        final loadedState = state as DriversLoaded;

        final allDrivers = [...loadedState.drivers, ...drivers];
        emit(
          DriversLoaded(
            drivers: allDrivers,
            allLoaded: drivers.length < _perPage,
            filters: filters,
            filter: _filter,
          ),
        );
        _currentPage++;
      } else {
        emit(
          DriversLoaded(
            drivers: drivers,
            allLoaded: drivers.length < _perPage,
            filters: filters,
            filter: _filter,
          ),
        );
        _currentPage = 2;
      }
    } catch (e) {
      debugPrint(e.toString());
      emit(DriversFailure(e.toString()));
    }
  }

  Future<void> setFilter(DriverFilter driverFilter) async {
    emit(DriversInitial());
    _filter = driverFilter;
    await getDrivers();
  }

  Future<void> getAll() async {
    emit(DriversLoading());
    final drivers = await _driverUseCase.getAll();

    emit(DriversLoaded(
      drivers: drivers,
      filters: filters,
      allLoaded: true,
      filter: _filter,
    ));
  }

  void subscribeUpdates() {
    _driverWebSocket.subscribe();
    streamSubscription = _driverWebSocket.stream.listen((driver) async {
      final drivers = await _driverUseCase.getAll();

      emit(DriversLoaded(
          drivers: drivers,
          filters: filters,
          allLoaded: true,
          filter: _filter));
    });
  }

  void unsubscribeUpdates() => streamSubscription?.cancel();

  Future<bool> block(driverId) async {
    final blocked = await _driverUseCase.block(driverId);
    refresh();
    return blocked;
  }

  Future<void> refresh() async {
    emit(DriversInitial());

    getDrivers();
  }

  @override
  Future<void> close() {
    unsubscribeUpdates();
    return super.close();
  }
}
