part of 'menu_cubit.dart';

enum Menu {
  drivers,
  orders,
  gps,
  statistics;

  @override
  String toString() {
    return switch (this) {
      Menu.drivers => "Drivers",
      Menu.orders => "Orders",
      Menu.statistics => "Statistics",
      Menu.gps => "GPS",
    };
  }
}
