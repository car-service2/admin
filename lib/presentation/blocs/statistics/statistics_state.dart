part of 'statistics_cubit.dart';

@immutable
abstract class StatisticsState {}

class StatisticsInitial extends StatisticsState {}

class StatisticsLoaded extends StatisticsState {
  final StatisticEntity statistic;

  StatisticsLoaded(this.statistic);
}

class StatisticLoading extends StatisticsState {}

class StatisticsFailure extends StatisticsState {}
