import 'package:admin/domain/entities/driver.dart';
import 'package:admin/domain/entities/organization.dart';
import 'package:admin/domain/entities/statistic.dart';
import 'package:admin/utils/date_time_extention.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/usecases/statistic_use_case.dart';

part 'statistics_state.dart';

class StatisticsCubit extends Cubit<StatisticsState> {
  final StatisticUseCase _statisticUseCase;

  StatisticsCubit(this._statisticUseCase) : super(StatisticsInitial());

  Future<void> getDriverStatistics(
    DriverEntity driver,
    DateTime startDate,
    DateTime endDate,
  ) async {
    try {
      final statistic = await _statisticUseCase.driver(
        driverId: driver.id,
        startDate: startDate.format("yyyy-MM-dd"),
        endDate: endDate.format("yyyy-MM-dd"),
      );

      emit(StatisticsLoaded(statistic));
    } catch (e) {
      debugPrint(e.toString());
      emit(StatisticsFailure());
    }
  }

  Future<void> getOrganizationStatistics(
    OrganizationEntity organization,
    DateTime startDate,
    DateTime endDate,
  ) async {
    try {
      final statistic = await _statisticUseCase.organization(
        organizationId: organization.id,
        startDate: startDate.format("yyyy-MM-dd"),
        endDate: endDate.format("yyyy-MM-dd"),
      );

      emit(StatisticsLoaded(statistic));
    } catch (e) {
      debugPrint(e.toString());
      emit(StatisticsFailure());
    }
  }
}
