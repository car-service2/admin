part of 'addresses_cubit.dart';

@immutable
abstract class AddressesState {}

class AddressesInitial extends AddressesState {}

class AddressesLoading extends AddressesState {}

class AddressesFailure extends AddressesState {
  final String message;

  AddressesFailure(this.message);
}

class AddressesLoaded extends AddressesState {
  final List<AddressEntity> addresses;

  AddressesLoaded(this.addresses);
}
