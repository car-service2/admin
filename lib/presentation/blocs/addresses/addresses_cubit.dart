import 'package:admin/domain/entities/address.dart';
import 'package:admin/domain/usecases/address_use_case.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

part 'addresses_state.dart';

class AddressesCubit extends Cubit<AddressesState> {
  final AddressesUseCase _addressUseCase;

  AddressesCubit(this._addressUseCase) : super(AddressesInitial());

  Future<void> load() async {
    try {
      emit(AddressesLoading());
      final addresses = await _addressUseCase.getAddresses();

      emit(AddressesLoaded(addresses));
    } catch (e) {
      debugPrint(e.toString());
      emit(AddressesFailure(e.toString()));
    }
  }
}
