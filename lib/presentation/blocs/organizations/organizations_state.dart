part of 'organizations_cubit.dart';

@immutable
abstract class OrganizationsState {}

class OrganizationsInitial extends OrganizationsState {}

class OrganizationsLoading extends OrganizationsState {}

class OrganizationsLoaded extends OrganizationsState {
  final List<OrganizationEntity> organizations;

  OrganizationsLoaded(this.organizations);
}

class OrganizationsFailure extends OrganizationsState {}
