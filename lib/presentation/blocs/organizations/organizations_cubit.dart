import 'package:admin/domain/entities/organization.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/usecases/organizations_use_case.dart';

part 'organizations_state.dart';

class OrganizationsCubit extends Cubit<OrganizationsState> {
  final OrganizationsUseCase _organizationsUseCase;

  OrganizationsCubit(this._organizationsUseCase)
      : super(OrganizationsInitial());

  Future<void> load() async {
    try {
      emit(OrganizationsLoading());
      final organizations = await _organizationsUseCase.getAll();

      emit(OrganizationsLoaded(organizations));
    } catch (e) {
      debugPrint(e.toString());
      emit(OrganizationsFailure());
    }
  }
}
