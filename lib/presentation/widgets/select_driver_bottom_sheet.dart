import 'dart:async';

import 'package:admin/domain/entities/address.dart';
import 'package:admin/domain/entities/driver.dart';
import 'package:admin/presentation/blocs/addresses/addresses_cubit.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter_cubit.dart';
import 'package:admin/presentation/blocs/drivers/drivers_cubit.dart';
import 'package:admin/presentation/pages/drivers_page/widgets/address_list.dart';
import 'package:admin/presentation/theme/app_theme.dart';
import 'package:admin/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../pages/drivers_page/widgets/driver_list.dart';
import '../pages/drivers_page/widgets/filter_bottom_sheet.dart';
import 'search_input.dart';

class SelectDriverBottomSheet extends StatefulWidget {
  const SelectDriverBottomSheet({super.key});

  static Future<T?> show<T>(context) async {
    return await showModalBottomSheet<T>(
      backgroundColor: AppColors.background,
      isScrollControlled: true,
      useSafeArea: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      showDragHandle: true,
      context: context,
      builder: (_) {
        final driversCubit = locator.get<DriversCubit>();
        final addressesCubit = locator.get<AddressesCubit>();
        final driverFilterCubit = DriverFilterCubit(
          driversCubit,
          addressesCubit,
        );
        return MultiBlocProvider(
          providers: [
            BlocProvider.value(value: driversCubit),
            BlocProvider.value(value: addressesCubit),
            BlocProvider.value(value: driverFilterCubit),
          ],
          child: const SelectDriverBottomSheet(),
        );
      },
    );
  }

  @override
  State<SelectDriverBottomSheet> createState() =>
      _SelectDriverBottomSheetState();
}

class _SelectDriverBottomSheetState extends State<SelectDriverBottomSheet> {
  var searchController = TextEditingController();
  Timer? _debounceTimer;

  @override
  void dispose() {
    searchController.dispose();
    _debounceTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          /// search and filter
          SizedBox(
            height: 52,
            child: Row(
              children: [
                Expanded(
                  child: SearchInput(
                    controller: searchController,
                    hint: "Search a driver",
                    onSearchTextChanged: _onSearchTextChanged,
                  ),
                ),
                const SizedBox(width: 10),
                BlocBuilder<DriverFilterCubit, DriverFilter>(
                  builder: (context, state) {
                    Map<String, dynamic> filters = state.toQueryParam();
                    bool isFilterApplied = filters.keys
                        .where((element) => element != 'search')
                        .isNotEmpty;
                    return GestureDetector(
                      onTap: () {
                        FilterBottomSheet.show(context, filters);
                      },
                      child: Container(
                        padding: const EdgeInsets.all(14),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color:
                              !isFilterApplied ? Colors.white : Colors.yellow,
                        ),
                        child: SvgPicture.asset("assets/icons/filter.svg"),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),

          SizedBox.fromSize(
            size: const Size.fromHeight(50),
            child: BlocBuilder<AddressesCubit, AddressesState>(
              bloc: BlocProvider.of<AddressesCubit>(context)..load(),
              builder: (context, state) {
                List<AddressEntity> addresses = [];

                if (state is AddressesLoaded) {
                  addresses = state.addresses;
                }

                return BlocBuilder<DriverFilterCubit, DriverFilter>(
                  builder: (context, state) {
                    return AddressList(
                      addresses: addresses,
                      selectedId: state.addressId,
                      onSelected: (AddressEntity selectedAddress) {
                        BlocProvider.of<DriverFilterCubit>(context)
                            .setAddress(selectedAddress.id);
                      },
                    );
                  },
                );
              },
            ),
          ),
          const SizedBox(height: 3),

          const SizedBox(height: 15),

          /// driver list
          DriversList(
            onSelectDriver: (DriverEntity driver) {
              Navigator.pop<DriverEntity>(context, driver);
            },
          ),
        ],
      ),
    );
  }

  void _onSearchTextChanged(String text) {
    _debounceTimer?.cancel();

    // Создаем новый таймер, чтобы вызвать обработчик через 500 миллисекунд
    _debounceTimer = Timer(const Duration(milliseconds: 500), () {
      // Здесь можно выполнить ваш поиск или другие действия с текстом
      debugPrint("Выполняем поиск $text");
      BlocProvider.of<DriverFilterCubit>(context).setSearch(text);
    });
  }
}
