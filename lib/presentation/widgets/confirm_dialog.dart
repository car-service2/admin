import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ConfirmDialog {
  static Future<void> show({
    required context,
    required String title,
    required String content,
    required VoidCallback onYes,
  }) async {
    await showCupertinoDialog(
      context: context,
      builder: (_) {
        return AlertDialog.adaptive(
          title: Text(title),
          content: Text(content),
          actions: [
            TextButton(
              onPressed: () {
                onYes();
                Navigator.pop(context);
              },
              child: const Text("Yes"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("No"),
            )
          ],
        );
      },
    );
  }
}
