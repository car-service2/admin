import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../blocs/menu/menu_cubit.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(30.0),
        topRight: Radius.circular(30.0),
      ),
      child: BlocBuilder<MenuCubit, Menu>(
        builder: (context, menu) {
          return BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: Menu.values.indexOf(menu),
            onTap: (index) {
              BlocProvider.of<MenuCubit>(context)
                  .set(Menu.values.elementAt(index));
            },
            items: [
              BottomNavigationBarItem(
                label: "Drivers",
                icon: SvgPicture.asset("assets/icons/driver.svg"),
                activeIcon: SvgPicture.asset("assets/icons/driver-active.svg"),
              ),
              BottomNavigationBarItem(
                label: "Orders",
                icon: SvgPicture.asset("assets/icons/order.svg"),
                activeIcon: SvgPicture.asset("assets/icons/order-active.svg"),
              ),
              BottomNavigationBarItem(
                label: "GPS",
                icon: SvgPicture.asset("assets/icons/gps.svg"),
                activeIcon: SvgPicture.asset("assets/icons/gps-active.svg"),
              ),
              BottomNavigationBarItem(
                label: "Statistic",
                icon: SvgPicture.asset("assets/icons/statistic.svg"),
                activeIcon:
                    SvgPicture.asset("assets/icons/statistic-active.svg"),
              ),
            ],
          );
        },
      ),
    );
  }
}
