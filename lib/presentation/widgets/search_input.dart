import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchInput extends StatefulWidget {
  final TextEditingController controller;
  final String hint;
  final void Function(String search) onSearchTextChanged;

  const SearchInput({
    super.key,
    required this.controller,
    required this.hint,
    required this.onSearchTextChanged,
  });

  @override
  State<SearchInput> createState() => _SearchInputState();
}

class _SearchInputState extends State<SearchInput> {
  @override
  Widget build(BuildContext context) {
    return CupertinoSearchTextField(
      backgroundColor: Colors.white,
      controller: widget.controller,
      onChanged: widget.onSearchTextChanged,
      borderRadius: BorderRadius.circular(16),
      padding: const EdgeInsets.symmetric(vertical: 14),
      prefixInsets: const EdgeInsets.symmetric(horizontal: 10),
      itemSize: 25,
    );
  }
}
