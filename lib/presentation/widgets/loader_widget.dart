import 'package:flutter/material.dart';

class LoaderWidget extends StatelessWidget {
  final Color? color;

  const LoaderWidget({
    super.key,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      color: color ?? Colors.white,
      strokeWidth: 1.5,
    );
  }
}
