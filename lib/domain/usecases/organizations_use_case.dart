import 'package:admin/domain/entities/organization.dart';
import 'package:admin/domain/repositories/organization_repository.dart';

class OrganizationsUseCase {
  final OrganizationRepository organizationRepository;

  OrganizationsUseCase(this.organizationRepository);

  Future<List<OrganizationEntity>> getAll() async {
    return await organizationRepository.getAll();
  }
}
