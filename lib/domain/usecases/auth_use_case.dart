import 'package:admin/domain/repositories/auth_repository.dart';

import '../entities/user.dart';

class AuthUseCase {
  final AuthRepository _authRepository;

  AuthUseCase(this._authRepository);

  Future<UserEntity> getUser() async {
    return await _authRepository.getUser();
  }
}
