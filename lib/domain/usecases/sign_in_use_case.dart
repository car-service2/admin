import 'package:admin/data/datasources/interfaces/token_datasource.dart';

import '../repositories/auth_repository.dart';

class SignInUseCase {
  final AuthRepository _authRepository;
  final TokenDataSource _tokenDataSource;

  SignInUseCase(this._authRepository, this._tokenDataSource);

  Future<bool> signIn({
    required String email,
    required String password,
  }) async {
    final token = await _authRepository.logIn(
      email: email,
      password: password,
    );

    return _tokenDataSource.setToken(token.accessToken);
  }
}
