import 'package:admin/domain/entities/statistic.dart';
import 'package:admin/domain/repositories/statistics_repository.dart';

abstract class StatisticUseCase {
  Future<StatisticEntity> driver({
    required int driverId,
    required String startDate,
    required String endDate,
  });

  Future<StatisticEntity> organization({
    required int organizationId,
    required String startDate,
    required String endDate,
  });
}

class StatisticUseCaseImpl implements StatisticUseCase {
  final StatisticsRepository _statisticsRepository;

  StatisticUseCaseImpl(this._statisticsRepository);

  @override
  Future<StatisticEntity> driver({
    required int driverId,
    required String startDate,
    required String endDate,
  }) async {
    return await _statisticsRepository.driver(
      driverId: driverId,
      startDate: startDate,
      endDate: endDate,
    );
  }

  @override
  Future<StatisticEntity> organization({
    required int organizationId,
    required String startDate,
    required String endDate,
  }) async {
    return await _statisticsRepository.organization(
      organizationId: organizationId,
      startDate: startDate,
      endDate: endDate,
    );
  }
}
