import 'package:admin/domain/entities/driver.dart';
import 'package:admin/domain/repositories/orders_repository.dart';

import '../entities/order.dart';

class OrdersUseCase {
  final OrdersRepository _ordersRepository;

  OrdersUseCase(this._ordersRepository);

  Future<List<OrderEntity>> getOrders({
    required Map<String, dynamic> queryParams,
  }) async {
    return await _ordersRepository.getOrders(
      queryParams: queryParams,
    );
  }

  Future<List<OrderEntity>> getDriverCurrentOrders(DriverEntity driver) async {
    return await _ordersRepository.getDriverCurrentOrders(driver.id);
  }

  Future<void> delete(int id) async {
    await _ordersRepository.delete(id);
  }

  Future<OrderEntity> create(OrderEntity order) async {
    return await _ordersRepository.create(order);
  }

  Future<OrderEntity> update(OrderEntity order) async {
    return await _ordersRepository.update(order);
  }
}
