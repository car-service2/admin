import 'package:admin/domain/entities/driver.dart';
import 'package:admin/domain/repositories/drivers_repository.dart';

class DriversUseCase {
  final DriversRepository _driversRepository;

  DriversUseCase(this._driversRepository);

  Future<List<DriverEntity>> getDrivers({
    required Map<String, dynamic> queryParams,
  }) async {
    return await _driversRepository.getPaginated(
      queryParams: queryParams,
    );
  }

  Future<List<DriverEntity>> getAll() async {
    return await _driversRepository.getAll();
  }

  Future<bool> block(id) async => _driversRepository.block(id);
}
