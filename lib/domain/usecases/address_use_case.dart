import 'package:admin/domain/entities/address.dart';

import '../repositories/address_repository.dart';

class AddressesUseCase {
  final AddressRepository _addressRepository;

  AddressesUseCase(this._addressRepository);

  Future<List<AddressEntity>> getAddresses() async {
    return _addressRepository.getAddresses();
  }
}
