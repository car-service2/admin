import 'package:admin/domain/entities/driver.dart';
import 'package:admin/domain/entities/organization.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../data/models/waypoint.dart';

class OrderEntity {
  final int id;
  final DriverEntity? driver;
  final DriverEntity? canceledDriver;
  final List<Waypoint> waypoints;
  final int date;
  final OrderStatusEntity? status;
  final String? customerName;
  final String customerPhone;
  final num? orderValue;
  final num? orderCreditValue;
  final num? orderTollValue;
  final String paymentType;
  final OrganizationEntity? organization;
  final bool important;
  final bool toll;
  final String? description;
  final bool female;

  DateTime get dateTime => DateTime.fromMillisecondsSinceEpoch(date * 1000);

  OrderEntity({
    required this.id,
    this.driver,
    this.canceledDriver,
    required this.waypoints,
    required this.date,
    required this.status,
    required this.customerName,
    required this.customerPhone,
    required this.orderValue,
    required this.orderCreditValue,
    required this.orderTollValue,
    required this.paymentType,
    required this.organization,
    required this.important,
    required this.toll,
    this.description,
    required this.female,
  });

  String get createdAt {
    final date = DateTime.fromMillisecondsSinceEpoch(this.date * 1000);
    const String format = 'MMM d, yyyy | h:mm a';
    final DateFormat formatter = DateFormat(format);
    return formatter.format(date);
  }

  get gender => female ? 'female' : 'male';

  List<String> get tags {
    List<String> tags = [createdAt];

    if (driver?.carType != null) {
      tags.add(driver!.carInfo);
    }

    if (toll) {
      tags.add("toll");
    }

    if (important) {
      tags.add('important');
    }

    tags.add(gender);

    return tags;
  }

  DateTime? parseDateTime(String dateString) {
    try {
      // Split the date string into time and date parts
      List<String> timeAndDate = dateString.split(" ");
      String timePart = timeAndDate[1];
      String datePart = timeAndDate[0];

      // Split the time part into hours, minutes, and seconds
      List<String> timeParts = timePart.split(":");
      int hours = int.parse(timeParts[0]);
      int minutes = int.parse(timeParts[1]);
      int seconds = int.parse(timeParts[2]);

      // Split the date part into day, month, and year
      List<String> dateParts = datePart.split("-");
      int day = int.parse(dateParts[0]);
      int month = int.parse(dateParts[1]);
      int year = int.parse(dateParts[2]);

      // Create the DateTime object
      DateTime dateTime = DateTime(year, month, day, hours, minutes, seconds);

      return dateTime;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  OrderEntity copyWith({
    int? id,
    DriverEntity? driver,
    List<Waypoint>? waypoints,
    int? date,
    OrderStatusEntity? status,
    String? customerName,
    String? customerPhone,
    double? orderValue,
    double? orderCreditValue,
    double? orderTollValue,
    String? paymentType,
    OrganizationEntity? organization,
    bool? important,
    bool? toll,
    String? description,
    bool? female,
  }) {
    return OrderEntity(
      id: id ?? this.id,
      driver: driver ?? this.driver,
      waypoints: waypoints ?? this.waypoints,
      date: date ?? this.date,
      status: status ?? this.status,
      customerName: customerName ?? this.customerName,
      customerPhone: customerPhone ?? this.customerPhone,
      orderValue: orderValue ?? this.orderValue,
      orderCreditValue: orderCreditValue ?? this.orderCreditValue,
      orderTollValue: orderTollValue ?? this.orderTollValue,
      paymentType: paymentType ?? this.paymentType,
      organization: organization ?? this.organization,
      important: important ?? this.important,
      toll: toll ?? this.toll,
      description: description ?? this.description,
      female: female ?? this.female,
    );
  }
}

enum OrderSortEntity {
  asc,
  desc,
}

enum OrderStatusEntity {
  all,
  saved,
  pending,
  accept,
  inProgress,
  success,
  canceled;

  @override
  String toString() {
    return switch (this) {
      OrderStatusEntity.all => "All",
      OrderStatusEntity.saved => "Saved",
      OrderStatusEntity.pending => "Pending",
      OrderStatusEntity.accept => "Accepted",
      OrderStatusEntity.inProgress => "In Progress",
      OrderStatusEntity.success => "Success",
      OrderStatusEntity.canceled => "Canceled",
    };
  }

  factory OrderStatusEntity.fromString(String status) {
    return switch (status) {
      "success" => OrderStatusEntity.success,
      "canceled" => OrderStatusEntity.canceled,
      "saved" => OrderStatusEntity.saved,
      "in_progress" => OrderStatusEntity.inProgress,
      "accept" => OrderStatusEntity.accept,
      "pending" => OrderStatusEntity.pending,
      _ => OrderStatusEntity.pending
    };
  }

  String toQueryParam() {
    return switch (this) {
      OrderStatusEntity.all => "",
      OrderStatusEntity.saved => "saved",
      OrderStatusEntity.pending => "pending",
      OrderStatusEntity.accept => "accept",
      OrderStatusEntity.inProgress => "in_progress",
      OrderStatusEntity.success => "success",
      OrderStatusEntity.canceled => "canceled",
    };
  }

  Color get color {
    return switch (this) {
      OrderStatusEntity.all => Colors.white,
      OrderStatusEntity.saved => Colors.indigoAccent,
      OrderStatusEntity.pending => Colors.yellow.shade500,
      OrderStatusEntity.accept => Colors.amber,
      OrderStatusEntity.inProgress => Colors.greenAccent,
      OrderStatusEntity.success => Colors.green.shade500,
      OrderStatusEntity.canceled => Colors.red,
    };
  }

  Color get foreground {
    Color getContrastingColor(Color backgroundColor) {
      // Извлечение компонентов цвета фона
      int red = backgroundColor.red;
      int green = backgroundColor.green;
      int blue = backgroundColor.blue;

      // Рассчитываем яркость цвета фона (0-255), чем выше значение, тем светлее цвет
      double brightness = (red * 0.299 + green * 0.587 + blue * 0.114);

      // Если яркость цвета фона больше половины максимального значения (255/2),
      // то выбираем черный цвет, иначе выбираем белый цвет
      return brightness > 255 / 2 ? Colors.black : Colors.white;
    }

    return getContrastingColor(color);
  }
}
