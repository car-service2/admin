class AddressEntity {
  final int id;
  final String name;
  final String? driversCount;

  AddressEntity({
    required this.id,
    required this.name,
    this.driversCount,
  });
}
