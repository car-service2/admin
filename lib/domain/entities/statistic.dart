class StatisticEntity {
  final num count;
  final num credit;
  final num cash;
  final num total;
  final num percentage;
  final num toll;

  StatisticEntity({
    required this.count,
    required this.credit,
    required this.cash,
    required this.total,
    required this.percentage,
    required this.toll,
  });
}
