class OrganizationEntity {
  final int id;
  final String name;

  OrganizationEntity({
    required this.id,
    required this.name,
  });
}