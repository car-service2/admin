import 'package:admin/utils/date_time_extention.dart';
import 'package:intl/intl.dart';

class DriverEntity {
  final int id;
  final bool blocked;
  final String name;
  final String login;
  final int phone;
  final int? seats;
  final String image;
  final String gender;
  final bool inLine;
  final String percent;
  final String? stopTime;
  final String? lat;
  final String? long;
  final CarTypeEntity? carType;
  final String? createdAt;

  String get carInfo {
    if (carType != null) {
      return "${carType?.name} ${seats != null ? "($seats)" : ""}";
    }

    return "";
  }

  DriverEntity({
    required this.id,
    required this.blocked,
    required this.name,
    required this.login,
    required this.phone,
    required this.image,
    required this.gender,
    required this.inLine,
    required this.percent,
    required this.stopTime,
    required this.lat,
    required this.long,
    required this.carType,
    required this.seats,
    required this.createdAt,
  });

  String? get since {
    if (createdAt != null) {
      // Парсим строку с датой в объект DateTime
      DateTime dateTime = DateTime.parse(createdAt!);

      // Форматируем дату в нужном формате
      String formattedDate = DateFormat('MMM d, yyyy').format(dateTime);

      return formattedDate;
    }
    return null;
  }

  factory DriverEntity.fromJson(Map<String, dynamic> json) {
    return DriverEntity(
      id: json['id'],
      blocked: json['blocked'] as bool,
      name: json['name'],
      login: json['login'],
      phone: json['phone'],
      image: json['image'],
      gender: json['gender'],
      inLine: json['in_line'],
      percent: json['percent'],
      stopTime: json['stop_time'],
      lat: json['lat'],
      long: json['long'],
      carType: CarTypeEntity.fromJson(json['car_type']),
      createdAt: json['created_at'],
      seats: int.tryParse(json['long'].toString()),
    );
  }

  double? get latitude => double.tryParse(lat ?? "");

  double? get longitude => double.tryParse(long ?? "");

  String get waiting {
    final timestamp1 = int.tryParse(this.stopTime ?? "0") ?? 0;
    final stopTime = DateTime.fromMillisecondsSinceEpoch(timestamp1 * 1000);
    final DateTime now = DateTime.now().toUtc();

    // Condition 1: at the location since (x) - stop_time <= now
    if (stopTime.isBefore(now)) {
      return "At the location.";
    } else {
      if (stopTime.difference(now).inDays <= 0) {
        return "Will arrive at ${stopTime.format('hh:mm a')}";
      } else {
        return "Will arrive at ${stopTime.format('yyyy-MM-dd')} ${stopTime.format('hh:mm a')}";
      }
    }
  }
}

class CarTypeEntity {
  int id;
  String name;
  String? description;

  CarTypeEntity({
    required this.id,
    required this.name,
    this.description,
  });

  factory CarTypeEntity.fromJson(Map<String, dynamic> json) {
    return CarTypeEntity(
      id: json['id'],
      name: json['name'],
      description: json['description'],
    );
  }
}
