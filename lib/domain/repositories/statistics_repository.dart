import 'package:admin/domain/entities/statistic.dart';

abstract class StatisticsRepository {
  Future<StatisticEntity> driver({
    required int driverId,
    required String startDate,
    required String endDate,
  });

  Future<StatisticEntity> organization({
    required int organizationId,
    required String startDate,
    required String endDate,
  });
}
