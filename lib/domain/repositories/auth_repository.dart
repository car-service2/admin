import '../entities/token.dart';
import '../entities/user.dart';

abstract class AuthRepository {
  Future<TokenEntity> logIn({
    required String email,
    required String password,
  });

  Future<UserEntity> getUser();
}
