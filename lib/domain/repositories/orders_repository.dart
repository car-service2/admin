import 'package:admin/domain/entities/order.dart';

abstract class OrdersRepository {
  Future<List<OrderEntity>> getOrders(
      {required Map<String, dynamic> queryParams});

  Future<void> delete(int id);

  Future<OrderEntity> create(OrderEntity order);

  Future<OrderEntity> update(OrderEntity order);

  Future<List<OrderEntity>> getDriverCurrentOrders(int id);
}
