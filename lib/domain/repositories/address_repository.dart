import 'package:admin/domain/entities/address.dart';

abstract class AddressRepository {
  Future<List<AddressEntity>> getAddresses();
}
