import '../../domain/entities/organization.dart';

abstract class OrganizationRepository {
  Future<List<OrganizationEntity>> getAll();
}
