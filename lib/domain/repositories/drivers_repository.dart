import '../entities/driver.dart';

abstract class DriversRepository {
  Future<List<DriverEntity>> getPaginated({
    required Map<String, dynamic> queryParams,
  });

  Future<List<DriverEntity>> getAll();

  Future<bool> block(id);
}
