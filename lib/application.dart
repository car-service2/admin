import 'package:admin/presentation/pages/main_page.dart';
import 'package:admin/presentation/pages/sign_in_page.dart';
import 'package:admin/presentation/widgets/loader_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'presentation/blocs/auth/auth_bloc.dart';
import 'presentation/theme/app_theme.dart';

class Application extends StatelessWidget {
  const Application({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is AuthError) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(state.message),
            ),
          );
        }
      },
      builder: (context, state) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Admin Application',
          builder: (context, child) {
            return MediaQuery(
              data:
                  MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
              child: child!,
            );
          },
          theme: AppTheme.themeData,
          home: _buildHomePage(state),
        );
      },
    );
  }

  Widget _buildHomePage(state) {
    if (state is AuthAuthenticated) {
      return const MainPage();
    }

    if (state is AuthUnauthenticated) {
      return const SignInPage();
    }

    return const Scaffold(
      body: Center(
        child: LoaderWidget(),
      ),
    );
  }
}
