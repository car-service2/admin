import 'package:admin/presentation/blocs/addresses/addresses_cubit.dart';
import 'package:admin/presentation/blocs/driver_filter/driver_filter_cubit.dart';
import 'package:admin/presentation/blocs/drivers/drivers_cubit.dart';
import 'package:admin/presentation/blocs/orders/orders_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'application.dart';
import 'presentation/blocs/auth/auth_bloc.dart';
import 'presentation/blocs/menu/menu_cubit.dart';
import 'presentation/blocs/sign_in/sign_in_cubit.dart';
import 'service_locator.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupServiceLocator();
  final authBloc = locator.get<AuthBloc>()..add(AuthCheckAuthenticationEvent());
  final driversCubit = locator.get<DriversCubit>();
  final addressCubit = locator.get<AddressesCubit>();
  final driverFilterCubit = DriverFilterCubit(driversCubit, addressCubit);
  final ordersCubit = locator.get<OrdersCubit>();

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => locator.get<MenuCubit>()),
        BlocProvider(create: (_) => locator.get<SignInCubit>()),
        BlocProvider(create: (_) => addressCubit),
        BlocProvider(create: (_) => ordersCubit),
        BlocProvider(create: (_) => driversCubit),
        BlocProvider(create: (_) => driverFilterCubit),
        BlocProvider(create: (_) => authBloc),
      ],
      child: const Application(),
    ),
  );
}
