import 'package:admin/data/datasources/remote/api/saved_orders_api_provider.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'data/datasources/interceptors/token_interceptor.dart';
import 'data/datasources/interfaces/address_datasource.dart';
import 'data/datasources/interfaces/auth_datasource.dart';
import 'data/datasources/interfaces/drivers_datasource.dart';
import 'data/datasources/interfaces/orders_datasource.dart';
import 'data/datasources/interfaces/organizations_datasource.dart';
import 'data/datasources/interfaces/statistics_datasource.dart';
import 'data/datasources/local/jwt_token_storage.dart';
import 'data/datasources/remote/api/address_api_provider.dart';
import 'data/datasources/remote/api/auth_api_provider.dart';
import 'data/datasources/remote/api/drivers_api_provider.dart';
import 'data/datasources/remote/api/orders_api_provider.dart';
import 'data/datasources/remote/api/organizations_api_provider.dart';
import 'data/datasources/remote/api/statistics_api_provider.dart';
import 'data/datasources/remote/websockets/driver_websocket.dart';
import 'data/mappers/mapper.dart';
import 'data/repositories/address_repository.dart';
import 'data/repositories/auth_repository.dart';
import 'data/repositories/drivers_repository.dart';
import 'data/repositories/orders_repository.dart';
import 'data/repositories/organization_repository.dart';
import 'data/repositories/statistics_repository.dart';
import 'domain/repositories/address_repository.dart';
import 'domain/repositories/auth_repository.dart';
import 'domain/repositories/drivers_repository.dart';
import 'domain/repositories/orders_repository.dart';
import 'domain/repositories/organization_repository.dart';
import 'domain/repositories/statistics_repository.dart';
import 'domain/usecases/address_use_case.dart';
import 'domain/usecases/auth_use_case.dart';
import 'domain/usecases/drivers_use_case.dart';
import 'domain/usecases/orders_use_case.dart';
import 'domain/usecases/organizations_use_case.dart';
import 'domain/usecases/sign_in_use_case.dart';
import 'domain/usecases/statistic_use_case.dart';
import 'presentation/blocs/addresses/addresses_cubit.dart';
import 'presentation/blocs/auth/auth_bloc.dart';
import 'presentation/blocs/create_order/create_order_cubit.dart';
import 'presentation/blocs/drivers/drivers_cubit.dart';
import 'presentation/blocs/menu/menu_cubit.dart';
import 'presentation/blocs/orders/orders_cubit.dart';
import 'presentation/blocs/organizations/organizations_cubit.dart';
import 'presentation/blocs/sign_in/sign_in_cubit.dart';
import 'presentation/blocs/statistics/statistics_cubit.dart';

final locator = GetIt.instance;
const apiBaseUrl = 'http://arideinc.com';
const wsUrl =
    'ws://arideinc.com:6001/app/pusher_key?protocol=7&client=js&version=4.3.1&flash=false';

/// Регистрируем зависимости
Future<void> setupServiceLocator() async {
  final sharedPreferences = await SharedPreferences.getInstance();

  locator.registerFactory<JwtTokenStorage>(
      () => JwtTokenStorage(sharedPreferences));

  final dio = Dio()
    ..interceptors.add(TokenInterceptor(locator.get<JwtTokenStorage>()))
    ..interceptors.add(
      PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90,
      ),
    );

  locator.registerLazySingleton<Dio>(() => dio);

  /// data sources
  locator.registerLazySingleton<AuthDataSource>(
    () => AuthApiProvider(locator<Dio>(), apiBaseUrl),
  );
  locator.registerLazySingleton<DriverDataSource>(
    () => DriverApiProvider(locator<Dio>(), apiBaseUrl),
  );
  locator.registerLazySingleton<AddressDataSource>(
    () => AddressApiProvider(locator.get<Dio>(), apiBaseUrl),
  );
  locator.registerLazySingleton<OrderDataSource>(
    () => OrdersApiProvider(locator.get<Dio>(), apiBaseUrl),
  );
  locator.registerLazySingleton<OrganizationsDataSource>(
    () => OrganizationsApiProvider(locator.get<Dio>(), apiBaseUrl),
  );
  locator.registerFactory<StatisticsDataSource>(
    () => StatisticsApiProvider(locator.get<Dio>(), apiBaseUrl),
  );

  /// mappers
  locator.registerLazySingleton<UserMapper>(() => UserMapper());
  locator.registerLazySingleton<TokenMapper>(() => TokenMapper());
  locator.registerLazySingleton<DriverMapper>(() => DriverMapper());
  locator.registerLazySingleton<AddressMapper>(() => AddressMapper());
  locator.registerLazySingleton<OrganizationMapper>(() => OrganizationMapper());
  locator.registerLazySingleton<OrderMapper>(
    () => OrderMapper(
      locator.get<OrganizationMapper>(),
      locator.get<DriverMapper>(),
    ),
  );

  /// repositories
  locator.registerLazySingleton<AuthRepository>(
    () => AuthRepositoryImpl(
      locator<AuthDataSource>(),
      locator<UserMapper>(),
      locator<TokenMapper>(),
    ),
  );
  locator.registerFactory<DriversRepository>(
    () => DriversRepositoryImpl(
      locator.get<DriverDataSource>(),
      locator.get<DriverMapper>(),
    ),
  );
  locator.registerFactory<AddressRepository>(
    () => AddressRepositoryImpl(
      locator.get<AddressDataSource>(),
      locator.get<AddressMapper>(),
    ),
  );
  locator.registerFactory<OrdersRepository>(
    () => OrdersRepositoryImpl(
      locator.get<OrderDataSource>(),
      locator.get<OrderMapper>(),
    ),
  );
  locator.registerFactory<OrganizationRepository>(
    () => OrganizationRepositoryImpl(
      locator.get<OrganizationsDataSource>(),
      locator.get<OrganizationMapper>(),
    ),
  );
  locator.registerFactory<StatisticsRepository>(
    () => StatisticsRepositoryImpl(locator.get<StatisticsDataSource>()),
  );

  /// use cases
  locator.registerFactory<AuthUseCase>(
    () => AuthUseCase(
      locator.get<AuthRepository>(),
    ),
  );
  locator.registerFactory<SignInUseCase>(
    () => SignInUseCase(
      locator.get<AuthRepository>(),
      locator.get<JwtTokenStorage>(),
    ),
  );
  locator.registerFactory<DriversUseCase>(
    () => DriversUseCase(locator.get<DriversRepository>()),
  );
  locator.registerFactory<AddressesUseCase>(
    () => AddressesUseCase(locator.get<AddressRepository>()),
  );
  locator.registerFactory<StatisticUseCase>(
    () => StatisticUseCaseImpl(locator.get<StatisticsRepository>()),
  );
  locator.registerFactory<OrdersUseCase>(
    () => OrdersUseCase(locator.get<OrdersRepository>()),
  );
  locator.registerFactory<OrganizationsUseCase>(
    () => OrganizationsUseCase(locator.get<OrganizationRepository>()),
  );

  /// websockets
  locator.registerFactory<DriverWebSocket>(
    () => DriverWebSocket(
      mapper: locator.get<DriverMapper>(),
      channel: "driver-update",
      event: "App\\Events\\DriverUpdated",
      wsUrl: wsUrl,
    ),
  );

  /// blocs & cubits
  locator.registerFactory<MenuCubit>(() => MenuCubit());
  locator.registerSingleton(
    AuthBloc(locator.get<AuthUseCase>()),
  );
  locator.registerSingleton(
    SignInCubit(
      locator.get<SignInUseCase>(),
      locator.get<AuthBloc>(),
    ),
  );
  locator.registerFactory(
    () => DriversCubit(
      locator.get<DriversUseCase>(),
      locator.get<DriverWebSocket>(),
    ),
  );
  locator.registerSingleton(
    AddressesCubit(locator.get<AddressesUseCase>()),
  );
  locator.registerSingleton(
    OrdersCubit(locator.get<OrdersUseCase>()),
  );
  locator.registerFactory(() => CreateOrderCubit(locator.get<OrdersUseCase>()));
  locator.registerFactory(
    () => CreateOrderCubit(
      OrdersUseCase(
        OrdersRepositoryImpl(
          SavedOrdersApiProvider(
            locator.get<Dio>(),
            apiBaseUrl,
          ),
          locator.get<OrderMapper>(),
        ),
      ),
    ),
    instanceName: 'CreateSavedOrderCubit',
  );
  locator.registerFactory(
    () => OrganizationsCubit(locator.get<OrganizationsUseCase>()),
  );
  locator.registerFactory(
    () => StatisticsCubit(locator.get<StatisticUseCase>()),
  );
}
